{-# LANGUAGE OverloadedStrings #-}
module Main (main, test) where

import qualified Data.HashMap.Strict as M.H
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import qualified Data.Text.IO as T.IO
import Data.Time.Calendar (Day)

import Data.LocallyUnique
import qualified Data.OrderedMap as M.O
import SVP.Calendar
import SVP.Change
import SVP.Course
import SVP.DayPlan
import SVP.GUI
import SVP.Schedule
import SVP.Topic

main :: IO ()
main = startGUI

testCourse :: Course
testCourse = Course (read "2000-01-01") (read "2000-03-30") testTopics testSchedule testSchedules
             testChanges

testTopics :: [TopicTree]
testTopics = [ SuperTopic "a"
               [ SuperTopic "b"
                 [ LeafTopic (TopicID 0) "c" 3
                 , LeafTopic (TopicID 1) "d" 5
                 ]
               , LeafTopic (TopicID 2) "e" 2
               ]
             , LeafTopic (TopicID 3) "f" 4
             ]

testSchedule :: WeekSchedule
testSchedule = WeekSchedule $ M.fromList
               [ ( Monday
                 , M.O.fromList $ zip (map DayKey $ newLocalKeys $ const True)
                   [ SimpleActivity ScheduledFree
                   , SimpleActivity ScheduledLesson
                   ]
                 )
               , ( Tuesday
                 , M.O.fromList
                   [ (DayKey $ newLocalKey $ const True
                     , AlternatingActivity ScheduledLesson ScheduledFree
                     )
                   ]
                 )
               ]

testSchedules :: M.Map Day WeekSchedule
testSchedules = M.fromList [ ( read "2000-02-01"
                             , WeekSchedule $ M.fromList
                                   [ ( Thursday
                                     , M.O.fromList [( DayKey $ newLocalKey $ const True
                                                     , SimpleActivity ScheduledLesson
                                                     )
                                                    ]
                                     )
                                   ]
                             )
                           ]

testChanges :: [Change]
testChanges = [changeActivity (read "2000-01-03") (DayKey $ newLocalKey $ const True)
               $ Lesson $ TopicID 3]

test :: IO ()
test = do
  let (topics, plan, unplannable) = planCourse testCourse
  mapM_ (T.IO.putStr . showPlanDay topics) $ M.toList plan
  print unplannable

showPlanDay :: M.H.HashMap TopicID Topic -> (Day, DayPlan) -> T.Text
showPlanDay topics (d, activity) = T.pack (show d) `T.append`
                                   ":\n" `T.append`
                                   showDayPlan topics activity

showDayPlan :: M.H.HashMap TopicID Topic -> DayPlan -> T.Text
showDayPlan topics = T.unlines . map (T.cons ' ' . showActivity topics) . M.O.elems

showActivity :: M.H.HashMap TopicID Topic -> ConcreteActivity -> T.Text
showActivity _ Free = "Free"
showActivity _ (Cancellation reason) = "Cancelled: " `T.append` reason
showActivity _ Buffer = "Buffer"
showActivity topics (Lesson topicID) = "Lesson: " `T.append`
                                       maybe "ERROR: Unknown topic"
                                             (\(Topic n _) -> T.intercalate "/" $ reverse n)
                                             (M.H.lookup topicID topics)
