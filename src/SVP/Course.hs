{-# LANGUAGE DeriveGeneric #-}
module SVP.Course ( Course(..)
                  , planCourse
                  ) where

import Data.Foldable (foldl')
import qualified Data.HashMap.Strict as M.H
import qualified Data.Map.Strict as M
import Data.Serialize (Serialize)
import Data.Time.Calendar (Day)

import qualified Data.OrderedMap as M.O
import GHC.Generics (Generic)
import SVP.Change
import SVP.DayPlan
import SVP.Schedule
import SVP.Topic

data Course = Course { courseBegin :: !Day
                     , courseEnd :: !Day
                     , courseTopics :: [TopicTree]
                     , courseFirstSchedule :: !WeekSchedule
                     , courseScheduleReplacements :: !(M.Map Day WeekSchedule)
                     , courseChanges :: [Change]
                     }
              deriving (Generic, Show, Eq)

instance Serialize Course

daysAsScheduled :: Course -> M.Map Day ConcreteDaySchedule
daysAsScheduled (Course begin end _ firstSched nextScheds _) =
  let currentRange currentBegin currentEnd currentSchedule =
          foldr (\d acc -> maybe acc (\concreteSchedule -> ((d, concreteSchedule) :) . acc) $
                           scheduleDay currentSchedule d) id [currentBegin..currentEnd]
      (lastPrefix, lastBegin, lastWeekSchedule) =
        foldl' (\(prefix, currentBegin, currentWeekSchedule)
                 (nextBegin, nextWeekSchedule) ->
                   let currentEnd = pred nextBegin
                   in ( prefix . currentRange currentBegin currentEnd currentWeekSchedule
                      , nextBegin
                      , nextWeekSchedule
                      )
               )
        (id, begin, firstSched) $ M.toAscList nextScheds
  in M.fromDistinctAscList $ lastPrefix . currentRange lastBegin end lastWeekSchedule $ []

daysAsScheduledWithChanges :: Course -> M.Map Day ChangedDaySchedule
daysAsScheduledWithChanges course@(Course _ _ _ _ _ cs) =
    applyChanges cs $ unchangedDay <$> daysAsScheduled course

planCourse :: Course -> (M.H.HashMap TopicID Topic, M.Map Day DayPlan, TopicToDo)
planCourse course@(Course _ _ topics _ _ _) =
    let days = daysAsScheduledWithChanges course
        toDo = subtractChangesFromToDos days $ topicsToDo topics
        (plan, undistributable) = distribute toDo days
    in ( topicIndex toDo
       , M.fromDistinctAscList plan
       , undistributable
       )

subtractChangesFromToDos :: M.Map Day ChangedDaySchedule -> TopicToDo -> TopicToDo
subtractChangesFromToDos = flip $ M.foldl' subtractDay
    where subtractDay = foldl' $ \toDo activity -> case activity of
                                                     ActivityAsScheduled _ -> toDo
                                                     ChangedActivity (Lesson topicID) ->
                                                         removeTopicToDo topicID toDo
                                                     ChangedActivity _ -> toDo

distribute :: TopicToDo -> M.Map Day ChangedDaySchedule -> ([(Day, DayPlan)], TopicToDo)
distribute toDo days =
  let (mkList, leftoverToDo) = foldl' (\(acc, toDo') (d, schedule) ->
                                         let (dayPlan, toDo'') = distributeDay schedule toDo'
                                         in (acc . ((d, dayPlan):), toDo'')
                                      ) (id, toDo) $ M.toAscList days
  in (mkList [], leftoverToDo)
    where distributeDay schedule toDo' =
              M.O.foldlWithKey' (\(plan, toDo'') key activity ->
                                     let (activity', toDo''') = concretize activity toDo''
                                     in (M.O.append key activity' plan, toDo''')
                                ) (M.O.empty, toDo') schedule
          concretize (ChangedActivity ac) toDo' = (ac, toDo')
          concretize (ActivityAsScheduled ScheduledFree) toDo' = (Free, toDo')
          concretize (ActivityAsScheduled ScheduledLesson) toDo' =
              maybe (Buffer, noTopicsToDo) (\(topicID, _, toDo'') -> (Lesson topicID, toDo'')
                                           ) $ nextTopicToDo toDo'
