{-# LANGUAGE OverloadedStrings, OverloadedLabels, LambdaCase, RecursiveDo, TupleSections #-}
module SVP.GUI (startGUI) where

import Control.Applicative (liftA2)
import Control.Monad ((>=>))
import Control.Monad.IO.Class (liftIO)
import qualified Data.GI.Gtk as Gtk
import qualified Data.Map as M
import qualified Data.List.NonEmpty as NE
import Data.Witherable ( mapMaybe
                       , wither
                       )
import GI.Gio (ApplicationFlags(ApplicationFlagsHandlesOpen))
import Reflex ( (<@>)
              , current
              , listHoldWithKey
              , mergeList
              , mergeMap
              , performEvent
              , switch
              )
import Reflex.GI.Gtk ( eventOnSignal
                     , eventOnSignal0
                     , runGtk
                     , runReflexGtk
                     )
import System.Environment ( getArgs
                          , getProgName
                          )
import System.Exit ( ExitCode(ExitFailure)
                   , exitFailure
                   , exitSuccess
                   , exitWith
                   )

import SVP.GUI.Common
import SVP.GUI.Instance
import SVP.GUI.Save

startGUI :: IO ()
startGUI =
  Gtk.applicationNew (Just "de.weltraumschlangen.Svp")
    [ ApplicationFlagsHandlesOpen
    ]
  >>= maybe exitFailure
  ( \application -> do
      argv <- liftA2 (:) getProgName getArgs
      runReflexGtk application (Just argv)
        ( do
            activated <- eventOnSignal0 application #activate
            newFromActivated <- performEvent $ ((Nothing,) <$> liftIO new) <$ activated
            opened <- eventOnSignal application #open $ \fire files _ -> fire files
            loadedFromOpen <- performEvent
              $ runGtk
              . wither ( #getPath
                         >=> maybe
                         (Nothing <$ reportError Nothing
                           "Opening remote files is not supported"
                         )
                         ( \localPath -> do
                             loaded <- loadSave' Nothing localPath
                             pure $ (Just localPath,) <$> loaded
                         )
                       )
              <$> opened
            rec
              let loaded = mconcat
                    [ loads
                    , pure <$> newFromActivated
                    , mapMaybe NE.nonEmpty loadedFromOpen
                    ]
                  freeKey = maybe (minBound::Word) fst . M.lookupMax <$> current loadsWithKey
                  loadedWithKey = (\freeKey' loaded' ->
                                     M.fromList $ zip [freeKey'..] $ map Just $ NE.toList loaded'
                                  ) <$> freeKey <@> loaded
              loadsWithKey <- listHoldWithKey M.empty (loadedWithKey <> deleted)
                $ \_ (path, save) -> startInstance application path save
              let loads = switch $ mergeList . map snd . M.elems <$> current loadsWithKey
                  deleted = switch $ mergeMap . M.map ((Nothing <$) . fst)
                            <$> current loadsWithKey
            pure ()
        )
        >>= \case
        0 -> exitSuccess
        n -> exitWith $ ExitFailure $ fromIntegral n
  )


