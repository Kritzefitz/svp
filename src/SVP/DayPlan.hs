{-# LANGUAGE DeriveGeneric #-}
module SVP.DayPlan ( ConcreteActivity(..)
                   , DayPlan
                   ) where

import qualified Data.Text as T

import qualified Data.OrderedMap as M
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import SVP.Orphans ()
import SVP.Schedule
import SVP.Topic

data ConcreteActivity = Free
                      | Cancellation !T.Text
                      | Lesson !TopicID
                      | Buffer
                        deriving (Generic, Show, Eq)

instance Serialize ConcreteActivity

type DayPlan = M.OrderedMap DayKey ConcreteActivity
