{-# LANGUAGE GeneralizedNewtypeDeriving, DeriveGeneric #-}
module SVP.Schedule ( ConcreteScheduledActivity(..)
                    , ConcreteDaySchedule
                    , DayKey(..)
                    , DaySchedule
                    , ScheduledActivity(..)
                    , WeekSchedule(..)
                    , concreteDaySchedule
                    , freeDayKey
                    , getWeekdaySchedule
                    , scheduleDay
                    ) where

import Data.Hashable (Hashable)
import qualified Data.Map.Strict as M
import Data.Serialize (Serialize)
import Data.Time.Calendar (Day)
import Data.Time.Calendar.WeekDate (toWeekDate)
import GHC.Generics (Generic)

import Data.LocallyUnique ( LocalKey
                          , newLocalKey
                          )
import qualified Data.OrderedMap as M.O
import SVP.Calendar

data ConcreteScheduledActivity = ScheduledFree
                               | ScheduledLesson
                                 deriving (Show, Generic, Eq)

instance Serialize ConcreteScheduledActivity

data ScheduledActivity = SimpleActivity !ConcreteScheduledActivity
                       | AlternatingActivity !ConcreteScheduledActivity !ConcreteScheduledActivity
                         deriving (Show, Generic, Eq)

instance Serialize ScheduledActivity

concreteActivity :: Day -> ScheduledActivity -> ConcreteScheduledActivity
concreteActivity _ (SimpleActivity a) = a
concreteActivity d (AlternatingActivity oddAc evenAc) = if even yearWeek
                                                        then evenAc
                                                        else oddAc
    where (_, yearWeek, _) = toWeekDate d

newtype DayKey = DayKey LocalKey
    deriving (Show, Eq, Ord, Hashable, Serialize)

type DaySchedule = M.O.OrderedMap DayKey ScheduledActivity

freeDayKey :: M.O.OrderedMap DayKey a -> DayKey
freeDayKey m = DayKey $ newLocalKey $ (not . flip M.O.member m) . DayKey

type ConcreteDaySchedule = M.O.OrderedMap DayKey ConcreteScheduledActivity

concreteDaySchedule :: Day -> DaySchedule -> ConcreteDaySchedule
concreteDaySchedule d = fmap (concreteActivity d)

newtype WeekSchedule = WeekSchedule (M.Map Weekday DaySchedule)
    deriving (Show, Generic, Eq)

instance Serialize WeekSchedule

getWeekdaySchedule :: WeekSchedule -> Weekday -> Maybe DaySchedule
getWeekdaySchedule (WeekSchedule m) d = M.lookup d m

scheduleDay :: WeekSchedule -> Day -> Maybe ConcreteDaySchedule
scheduleDay schedule d = concreteDaySchedule d <$> getWeekdaySchedule schedule (day2weekday d)
