{-# LANGUAGE DeriveGeneric #-}
module SVP.Change ( Change(Change, changeFrame, changeTo)
                  , ChangedActivity(..)
                  , ChangedDaySchedule
                  , TimeFrame(..)
                  , appliesTo
                  , applyChange
                  , applyChanges
                  , changeActivity
                  , unchangedActivity
                  , unchangedDay
                  ) where

import Data.Hashable ( Hashable
                     , hashWithSalt
                     )
import qualified Data.Map.Strict as M
import qualified Data.OrderedMap as M.O
import Data.Serialize (Serialize)
import Data.Time ( Day(ModifiedJulianDay)
                 , toModifiedJulianDay
                 )
import GHC.Generics (Generic)

import SVP.DayPlan
import SVP.Orphans ()
import SVP.Schedule

data ChangedActivity = ActivityAsScheduled ConcreteScheduledActivity
                     | ChangedActivity ConcreteActivity

unchangedActivity :: ConcreteScheduledActivity -> ChangedActivity
unchangedActivity = ActivityAsScheduled

type ChangedDaySchedule = M.O.OrderedMap DayKey ChangedActivity

unchangedDay :: ConcreteDaySchedule -> ChangedDaySchedule
unchangedDay = fmap unchangedActivity

data TimeFrame = FrameActivity !Day !DayKey
               | FrameDay !Day
               | FrameDays !Day !Day
              deriving (Generic, Show)

instance Eq TimeFrame where
  FrameActivity d1 dk1 == FrameActivity d2 dk2 = d1 == d2 && dk1 == dk2
  FrameDay d1 == FrameDay d2 = d1 == d2
  FrameDays s1 e1 == FrameDays s2 e2 = s1 == s2 && e1 == e2
  FrameDay d == FrameDays s e = d == s && d == e
  ds@FrameDays{} == d@FrameDay{} = d == ds
  _ == _ = False

-- | More specific TimeFrames are greater than less specific
-- TimeFrames. Equally specific TimeFrames are ordered by start date
-- and end date or day key.
instance Ord TimeFrame where
  FrameActivity d1 dk1 `compare` FrameActivity d2 dk2 =
    d1 `compare` d2 <> dk1 `compare` dk2
  FrameActivity{} `compare` _ = GT
  FrameDay{} `compare` FrameActivity{} = LT
  FrameDay d1 `compare` FrameDay d2 = d1 `compare` d2
  FrameDay d `compare` FrameDays s e
    | d == s && s == e = EQ
    | otherwise = GT
  FrameDays{} `compare` FrameActivity{} = LT
  FrameDays s e `compare` FrameDay d
    | d == s && s == e = EQ
    | otherwise = LT
  f1@(FrameDays s1 e1) `compare` f2@(FrameDays s2 e2)
    | s1 == e1 = FrameDay s1 `compare` f2
    | s2 == e2 = f1 `compare` FrameDay s2
    | otherwise = s1 `compare` s2 <> e1 `compare` e2

instance Serialize TimeFrame

instance Hashable TimeFrame where
  hashWithSalt s (FrameActivity (ModifiedJulianDay d) dk) = s `hashWithSalt`
                                                            d `hashWithSalt` dk
  hashWithSalt s (FrameDay (ModifiedJulianDay d)) = hashWithSalt s d
  hashWithSalt s (FrameDays b e)
    | b == e = hashWithSalt s (FrameDay b)
    | otherwise = s `hashWithSalt`
                  toModifiedJulianDay b `hashWithSalt` toModifiedJulianDay e

data Change = Change { changeFrame :: !TimeFrame
                     , changeTo :: !ConcreteActivity
                     }
            deriving (Generic, Show, Eq)

instance Serialize Change

applyChange :: Change -> M.Map Day ChangedDaySchedule -> M.Map Day ChangedDaySchedule
applyChange (Change timeFrame activity) =
             case timeFrame of
               (FrameActivity day key) -> M.adjust (M.O.update key $ ChangedActivity activity) day
               (FrameDay day) -> M.adjust (ChangedActivity activity <$) day
               (FrameDays firstDay lastDay) ->
                 foldr (\day -> (applyChange (Change (FrameDay day) activity) .)) id
                 [firstDay..lastDay]

applyChanges :: [Change] -> M.Map Day ChangedDaySchedule -> M.Map Day ChangedDaySchedule
applyChanges = foldr (\change -> (applyChange change .)) id

appliesTo :: Change -> TimeFrame -> Bool
(Change tf _) `appliesTo` tf' = tf == tf'

changeActivity :: Day -> DayKey -> ConcreteActivity -> Change
changeActivity d dk = Change (FrameActivity d dk)
