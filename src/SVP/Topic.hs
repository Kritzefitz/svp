{-# LANGUAGE GeneralizedNewtypeDeriving, DeriveGeneric #-}
module SVP.Topic ( Topic(..)
                 , TopicID(..)
                 , TopicToDo
                 , TopicTree(..)
                 , nextTopicToDo
                 , noTopicsToDo
                 , removeTopicToDo
                 , topicIndex
                 , topicsToDo
                 ) where

import Data.Hashable (Hashable)
import qualified Data.HashMap.Strict as M
import qualified Data.Sequence as S
import Data.Serialize (Serialize)
import qualified Data.Text as T
import GHC.Generics (Generic)
import SVP.Orphans ()

newtype TopicID = TopicID Word
    deriving (Show, Eq, Hashable, Serialize)

data TopicTree = SuperTopic !T.Text [TopicTree]
               | LeafTopic !TopicID !T.Text !Int
                 deriving (Show, Generic, Eq)

instance Serialize TopicTree

data Topic = Topic ![T.Text] !Int
             deriving (Show)

subtractLesson :: Topic -> Topic
subtractLesson (Topic n lessons) = Topic n $ pred lessons

data TopicToDo = TopicToDo !(S.Seq TopicID) !(M.HashMap TopicID Topic)
                 deriving (Show)

noTopicsToDo :: TopicToDo
noTopicsToDo = TopicToDo S.empty M.empty

topicsToDo :: [TopicTree] -> TopicToDo
topicsToDo = uncurry TopicToDo . indexMany []
    where indexOne path (SuperTopic n subs) = indexMany (n:path) subs
          indexOne path (LeafTopic tID n lessons) = ( S.singleton tID
                                                    , M.singleton tID $ Topic (n:path) lessons
                                                    )
          indexMany path = foldr (\topic (orderAcc, indexAcc) ->
                                      let (nIDs, nIdx) = indexOne path topic
                                      in (nIDs S.>< orderAcc, M.union nIdx indexAcc)
                                 ) (S.empty, M.empty)

removeTopicToDo :: TopicID -> TopicToDo -> TopicToDo
removeTopicToDo topicID (TopicToDo order index) =
    TopicToDo order $ M.adjust subtractLesson topicID index

topicIndex :: TopicToDo -> M.HashMap TopicID Topic
topicIndex (TopicToDo _ index) = index

nextTopicToDo :: TopicToDo -> Maybe (TopicID, [T.Text], TopicToDo)
nextTopicToDo (TopicToDo order index) =
    case S.viewl order of
      S.EmptyL -> Nothing
      tID S.:< rest -> case M.lookup tID index of
                         Nothing -> nextTopicToDo $ TopicToDo rest index
                         Just (Topic n lessons)
                             | lessons <= 0 -> nextTopicToDo $ TopicToDo rest $ M.delete tID index
                             | otherwise -> Just ( tID
                                                 , n
                                                 , TopicToDo order $ M.adjust subtractLesson tID index
                                                 )
