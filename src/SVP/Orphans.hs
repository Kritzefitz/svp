{-# OPTIONS_GHC -fno-warn-orphans #-}
module SVP.Orphans () where

import Control.Exception (displayException)
import Data.Serialize ( Serialize
                      , get
                      , put
                      )
import qualified Data.Text as T
import Data.Text.Encoding ( decodeUtf8'
                          , encodeUtf8
                          )
import Data.Time.Calendar (Day(ModifiedJulianDay))

instance Serialize Day where
  get = fmap ModifiedJulianDay get
  put (ModifiedJulianDay d) = put d

instance Serialize T.Text where
  get = get >>= \bytes -> case decodeUtf8' bytes of
    Left e -> fail $ displayException e
    Right t -> pure t
  put = put . encodeUtf8
