{-# LANGUAGE DeriveGeneric #-}
module SVP.Calendar ( Weekday(..)
                    , getToday
                    , day2weekday
                    , week
    ) where

import Data.Serialize (Serialize)
import Data.Time.LocalTime ( getZonedTime
                           , localDay
                           , zonedTimeToLocalTime
                           )
import Data.Time.Calendar (Day)
import Data.Time.Calendar.WeekDate (toWeekDate)
import GHC.Generics (Generic)
import Data.Hashable ( Hashable
                     , hashWithSalt
                     , hashUsing
                     )
    
data Weekday
    = Monday
    | Tuesday
    | Wednesday
    | Thursday
    | Friday
    | Saturday
    | Sunday
    deriving (Show, Eq, Ord, Bounded, Enum, Read, Generic)

instance Hashable Weekday where
    hashWithSalt = hashUsing fromEnum

instance Serialize Weekday

num2weekday :: Int -> Weekday
num2weekday =
    toEnum . (+(-1))

getToday :: IO Day
getToday =
    fmap (localDay . zonedTimeToLocalTime) getZonedTime

day2weekday :: Day -> Weekday
day2weekday =
    num2weekday . third . toWeekDate
    where
      third (_, _, x) = x

week :: [Weekday]
week = [Monday .. Sunday]
