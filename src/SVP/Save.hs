{-# LANGUAGE OverloadedStrings #-}
module SVP.Save ( SVPSave(..)
                , deserializeSave
                , loadSave
                , serializeSave
                , writeSave
                ) where

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as B.L
import Data.SafeCopy ( SafeCopy
                     , base
                     , contain
                     , getCopy
                     , kind
                     , putCopy
                     , safeGet
                     , safePut
                     , version
                     )
import Data.Serialize ( get
                      , getBytes
                      , put
                      , putByteString
                      , runGetLazy
                      , runPutLazy
                      )

import SVP.Course

newtype SVPSave = SVPSave {savedCourse :: Course}
  deriving (Eq)

instance SafeCopy SVPSave where
    version = 0
    kind = base
    -- A save file is always written by the same version of SVP, so
    -- tagging all data structures all the way down would mostly blow
    -- up the size of save files. To prevent this we only use SafeCopy
    -- for the first level (i.e. SVPSave) and use cereal below
    -- that. The tradeoff is that changes to structures deep in the
    -- save structure make copies of all the structures above that
    -- necessary.
    getCopy = contain $ getBytes (B.length saveSignature) >>= \readSignature ->
      if readSignature == saveSignature
      then fmap SVPSave get
      else fail "SVPSave has an invalid format signature"
    putCopy (SVPSave course) = contain $ putByteString saveSignature >> put course

-- SVP saves are tagged with a human readable commend and 16 bytes
-- that were originally chosen randomly. This signature should
-- differentiate SVP saves from other types of data.
-- NOTE: SafeCopy puts the version tag before our format signature.
randomSignature :: B.ByteString
randomSignature = B.pack [0x44, 0x7b, 0x87, 0xff, 0xac, 0x49, 0x91, 0xde,
                          0xbf, 0x3f, 0x6c, 0xe8, 0x6f, 0x9a, 0xd2, 0xc2]

saveSignature :: B.ByteString
saveSignature = "SVP data save" `B.append` randomSignature

serializeSave :: SVPSave -> B.L.ByteString
serializeSave = runPutLazy . safePut

writeSave :: SVPSave -> FilePath -> IO ()
writeSave save path = B.L.writeFile path $ serializeSave save

deserializeSave :: B.L.ByteString -> Either String SVPSave
deserializeSave = runGetLazy safeGet

loadSave :: FilePath -> IO (Either String SVPSave)
loadSave path = deserializeSave <$> B.L.readFile path
