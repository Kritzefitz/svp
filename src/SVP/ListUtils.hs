module SVP.ListUtils ( deleteBy
                     , deleteAt
                     ) where

deleteBy :: (a -> Bool) -> [a] -> [a]
deleteBy _ [] = []
deleteBy f (x:xs)
    | f x = xs
    | otherwise = x : deleteBy f xs

deleteAt :: Word -> [a] -> [a]
deleteAt _ [] = []
deleteAt 0 (_:xs) = xs
deleteAt n (x:xs) = x : deleteAt (n-1) xs
