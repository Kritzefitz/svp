{-# LANGUAGE RecordWildCards, RecursiveDo, OverloadedStrings, TupleSections, OverloadedLabels #-}
{-# LANGUAGE FlexibleContexts #-}
module SVP.GUI.Output (outputCourse) where

import Control.Applicative ( (<|>)
                           , empty
                           , liftA2
                           )
import Control.Monad ( join
                     , when
                     )
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe ( MaybeT(MaybeT)
                                 , runMaybeT
                                 )
import Data.Foldable ( foldlM
                     , foldl'
                     )
import Data.Function ((&))
import qualified Data.GI.Gtk as Gtk
import qualified Data.HashMap.Strict as M.H
import qualified Data.Map.Strict as M
import Data.Maybe ( fromJust
                  , fromMaybe
                  )
import qualified Data.Text as T
import qualified Data.Text.Lazy as T.L
import qualified Data.Text.Lazy.Builder as T.L.B
import Data.Time (Day)
import Data.Tree (Tree(Node))
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as V.M
import Reflex ( Dynamic
              , Event
              , (<@)
              , (<@>)
              , accum
              , current
              , ffilter
              , getPostBuild
              , holdDyn
              , leftmost
              , never
              , performEvent_
              , switch
              , updated
              )
import Reflex.Collection (listWithKey)
import Reflex.GI.Gtk ( MonadReflexGtk
                     , ReactiveAttrOp((:==))
                     , dynamicOnSignal
                     , eventOnSignal
                     , holdNotReadyWidget
                     , holdNotReadyDynamicWidget
                     , runGtk
                     , sink
                     , sinkBin
                     , sinkBoxUniform
                     )
import Reflex.Network ( networkHold
                      , networkView
                      )

import qualified Data.OrderedMap as M.O
import SVP.Change
import SVP.Course
import SVP.DayPlan
import SVP.GUI.Common
import SVP.GUI.TopicTree
import SVP.Schedule
import SVP.Topic

data InlinedActivity = IFree
                     | ICancellation !T.Text
                     | ILesson !Topic
                     | IBuffer
                     deriving (Show)

inlineActivity :: M.H.HashMap TopicID Topic -> ConcreteActivity -> InlinedActivity
inlineActivity _ Free = IFree
inlineActivity _ (Cancellation reason) = ICancellation reason
inlineActivity topics (Lesson topicID) = ILesson $ topics M.H.! topicID
inlineActivity _ Buffer = IBuffer

data OutputRow = DayHeader !Day
               | ActivityRow !Day {-# UNPACK #-}!DayKey
                 {-# UNPACK #-}!Int !InlinedActivity
               deriving (Show)

outputRowDay :: OutputRow -> Day
outputRowDay (DayHeader d) = d
outputRowDay (ActivityRow d _ _ _) = d

outputRowHour :: OutputRow -> Maybe Int
outputRowHour (DayHeader _) = Nothing
outputRowHour (ActivityRow _ _ h _) = Just h

outputRowActivity :: OutputRow -> Maybe InlinedActivity
outputRowActivity (DayHeader _) = Nothing
outputRowActivity (ActivityRow _ _ _ activity) = Just activity

newConcreteActivityWidget :: (MonadReflexGtk t m)
                          => Gtk.ForestStore TopicRow
                          -> Maybe ConcreteActivity
                          -> m (Dynamic t (Maybe ConcreteActivity), Gtk.Widget)
newConcreteActivityWidget model initialActivity = do
  grid <- runGtk $ Gtk.new Gtk.Grid []
  unchangedRadio <- runGtk $ Gtk.new Gtk.RadioButton [#label Gtk.:= "Unchanged"]
  cancellationRadio <- runGtk
    $ Gtk.radioButtonNewWithLabelFromWidget (Just unchangedRadio) "Cancellation"
  lessonRadio <- runGtk $ Gtk.radioButtonNewWithLabelFromWidget (Just cancellationRadio) "Lesson"
  cancellationEntry <- runGtk $ Gtk.new Gtk.Entry []
  (lessonWidget, lessonComboBox) <- runGtk $ lessonSelector model
  runGtk $ do
    #setSensitive cancellationEntry False
    #setSensitive lessonComboBox False
    #attach grid unchangedRadio 0 0 1 1
    #attach grid cancellationRadio 1 0 1 1
    #attach grid lessonRadio 2 0 1 1
    #attach grid cancellationEntry 1 1 1 1
    #attach grid lessonWidget 2 1 1 1
    #show unchangedRadio
    #show cancellationRadio
    #show lessonRadio
    #show cancellationEntry
    #show lessonWidget
  (initialCancellation, initialLesson) <-
    runGtk $ case initialActivity of
               Just l@(Lesson i) -> do
                 #setSensitive lessonComboBox True
                 #setActive lessonRadio True
                 treeStoreFind model ((==Just i) . topicRowID) >>=
                   maybe (error "initialLesson not in model")
                   (#setActiveIter lessonComboBox . Just)
                 pure (Cancellation "", Just l)
               Just c@(Cancellation t) -> do
                 #setSensitive cancellationEntry True
                 #setActive cancellationRadio True
                 #setText cancellationEntry t
                 pure (c, Nothing)
               Just _ -> error "Unsupported activity for newConcreteActivityWidget"
               Nothing -> do
                 #setActive unchangedRadio True
                 pure (Cancellation "", Nothing)
  cancellation <- dynamicOnSignal initialCancellation cancellationEntry #changed
                  $ (#getText cancellationEntry >>=) . (. Cancellation)
  lesson <- dynamicOnSignal initialLesson lessonComboBox #changed
                     ( runMaybeT
                       (do
                           selectedIter <-
                             boolPairToMaybeT $ #getActiveIter lessonComboBox
                           lift $ Lesson <$> do
                             topicRow <- Gtk.treeModelGetPath model selectedIter >>=
                               Gtk.forestStoreGetValue model
                             maybe (error "super topic selected as change target") pure $
                               topicRowID topicRow
                       ) >>=
                     )
  isUnchanged <- eventOnSignal unchangedRadio #toggled
                 (#getActive unchangedRadio >>=)
  isLesson <- eventOnSignal lessonRadio #toggled
              (#getActive lessonRadio >>=)
  sink lessonComboBox [#sensitive :== isLesson]
  isCancellation <- eventOnSignal cancellationRadio #toggled
                    (#getActive cancellationRadio >>=)
  sink cancellationEntry [#sensitive :== isCancellation]
  let initialOutput = case initialActivity of
                        Just Lesson{} -> lesson
                        Just Cancellation{} -> fmap Just cancellation
                        Nothing -> pure Nothing
                        _ -> error "Unsupported activity for newConcreteActivityWidget"
  selectedOutput <- holdDyn initialOutput $ leftmost
                    [ pure Nothing <$ ffilter id isUnchanged
                    , lesson <$ ffilter id isLesson
                    , fmap Just cancellation <$ ffilter id isCancellation
                    ]
  (join selectedOutput,) <$> Gtk.toWidget grid


lessonSelector :: Gtk.ForestStore TopicRow -> IO (Gtk.Widget, Gtk.ComboBox)
lessonSelector model = do
  comboBox <- Gtk.new Gtk.ComboBox [#model Gtk.:= model]
  _ <- addTextCellRenderer comboBox model topicRowName
  _ <- addTextCellRenderer comboBox model $ maybe "" (T.pack . show) . topicRowLessons
  (, comboBox) <$> Gtk.toWidget comboBox

treeStoreFind :: Gtk.ForestStore a -> (a -> Bool) -> IO (Maybe Gtk.TreeIter)
treeStoreFind store f = runMaybeT $ boolPairToMaybeT (Gtk.treeModelGetIterFirst store)
                        >>= MaybeT . iterFind
  where iterFind iter = do
          value <- Gtk.treeModelGetPath store iter >>= Gtk.forestStoreGetValue store
          if f value
            then pure $ Just iter
            else runMaybeT $
                 ( boolPairToMaybeT (Gtk.treeModelIterChildren store (Just iter)) >>=
                   MaybeT . iterFind
                 ) <|>
                 (do
                     hasNext <- Gtk.treeModelIterNext store iter
                     if hasNext
                       then MaybeT $ iterFind iter
                       else empty
                 )

boolPairToMaybe :: (Bool, a) -> Maybe a
boolPairToMaybe (True, x) = Just x
boolPairToMaybe (False, _) = Nothing

boolPairToMaybeT :: (Functor m) => m (Bool, a) -> MaybeT m a
boolPairToMaybeT = MaybeT . fmap boolPairToMaybe

outputCourse :: (MonadReflexGtk t m)
             => Gtk.Builder
             -> Gtk.ForestStore TopicRow
             -> Dynamic t Course
             -> [Change]
             -> m (Dynamic t [Change])
outputCourse builder topicStore course initialChanges = do
  outputView <- runGtk $ castB builder "result-table" Gtk.TreeView
  outputStore <- runGtk $ Gtk.forestStoreNew []
  runGtk $ Gtk.set outputView [#model Gtk.:= outputStore]
  runGtk $
    mapM_ (uncurry $ addTextColumn outputView outputStore)
      [ (T.pack . show . outputRowDay, "Day")
      , (maybe "" (T.pack . show) . outputRowHour, "Hour")
      , (maybe "" (T.pack . show) . outputRowActivity, "Activity")
      ]
  let plan = fmap planCourse course
  postBuild <- getPostBuild
  performEvent_ $ runGtk . updatePlanned outputStore <$> current plan <@ postBuild
  performEvent_ $ runGtk . updatePlanned outputStore <$> updated plan
  setupDetails builder topicStore outputStore outputView initialChanges
    ((\(x,_,_) -> x) <$> plan)

updatePlanned :: Gtk.ForestStore OutputRow
              -> (M.H.HashMap TopicID Topic, M.Map Day DayPlan, TopicToDo)
              -> IO ()
updatePlanned store (topics, days, unplanned) = do
  goUpdateDays 0 $ M.toAscList days
  print unplanned
    where goUpdateDays pos [] = Gtk.treePathNewFromIndices [pos] >>= clearFrom
          goUpdateDays pos allDays@((day, dayPlan) : rest) = do
            path <- Gtk.treePathNewFromIndices [pos]
            Gtk.forestStoreLookup store path >>= maybe
              insertAndUpdateAndContinue
              (\(Node displayedRow _) ->
                 case displayedRow of
                   -- an ActivityRow should never be in this
                   -- position. Remove it.
                   ActivityRow{} -> clearCurrentAndContinue path
                   DayHeader displayedDay ->
                     if day > displayedDay
                     -- The currently displayed day was removed.
                     then clearCurrentAndContinue path
                     else
                       -- If the day is not in the store, we insert it
                       -- and carry on as if it was there all along.
                       when (day < displayedDay) insertOnly
                       *> updateCurrentAndContinue
              )
              where insertOnly = do
                      rootPath <- Gtk.treePathNew
                      Gtk.forestStoreInsert store rootPath (fromIntegral pos) (DayHeader day)
                    updateCurrentAndContinue =
                      goUpdateActivities day pos (M.O.toList dayPlan)
                      *> goUpdateDays (succ pos) rest
                    insertAndUpdateAndContinue =
                      insertOnly *> updateCurrentAndContinue
                    clearCurrentAndContinue path =
                      Gtk.forestStoreRemove store path *> goUpdateDays pos allDays
          goUpdateActivities day dayPos =
            mapM_ (\(i, (dk, activity)) -> do
                      let row = ActivityRow day dk i $ inlineActivity topics activity
                      parentPath <- Gtk.treePathNewFromIndices [dayPos]
                      path <- Gtk.treePathNewFromIndices [dayPos, fromIntegral i]
                      Gtk.forestStoreLookup store path >>=
                        maybe
                        (Gtk.forestStoreInsert store parentPath i row)
                        (\_ -> Gtk.forestStoreSetValue store path row)
                  ) . zip [0..]
          clearFrom path = do
            success <- Gtk.forestStoreRemove store path
            if success
              then clearFrom path
              else pure ()

data DaySelection = DaySelection { daySelectionSelected :: !Bool
                                 , daySelectionHours :: {-# UNPACK #-}!(V.Vector HourSelection)
                                 , daySelectionDay :: !Day
                                 }
                  deriving (Show)

data HourSelection =
  HourSelection { hourSelectionSelected :: !Bool
                , hourSelectionDayKey :: {-# UNPACK #-}!DayKey
                , hourSelectionEnumerable :: {-# UNPACK #-}!Int
                , hourSelectionActivity :: !InlinedActivity
                }
  deriving (Show)

setupDetails :: (MonadReflexGtk t m)
             => Gtk.Builder
             -> Gtk.ForestStore TopicRow
             -> Gtk.ForestStore OutputRow
             -> Gtk.TreeView
             -> [Change]
             -> Dynamic t (M.H.HashMap TopicID Topic)
             -> m (Dynamic t [Change])
setupDetails builder topicStore outputStore outputView initialChanges topicIndex' = do
  outputSelection <- runGtk $ #getSelection outputView
  sideBox <- runGtk $ castB builder "plan-detail-box" Gtk.Box
  selection <- dynamicOnSignal [] outputSelection #changed
               (\fire ->
                   #getSelectedRows outputSelection >>=
                   orderSelection outputStore . fst >>=
                   fire
               )
  (changeWidget, changes) <- setupChangeInput topicStore initialChanges $ updated selection
  affectWidgets <- setupAffectingChanges topicIndex' selection changes
  sinkBoxUniform sideBox (liftA2 (:) changeWidget affectWidgets) False False 0 Gtk.PackTypeStart
  runGtk $ #show sideBox
  pure $ map (uncurry Change) . M.toList <$> changes

setupChangeInput :: (MonadReflexGtk t m)
                 => Gtk.ForestStore TopicRow
                 -> [Change]
                 -> Event t [DaySelection]
                 -> m (Dynamic t Gtk.Widget, Dynamic t (M.Map TimeFrame ConcreteActivity))
setupChangeInput topicStore initialChanges selection = mdo
  let changeFrameM = fmap eligibleForChange selection
      initialChangeMap = M.fromList $ map (\(Change f a) -> (f, a)) initialChanges
  changeWidget <- networkHold (changeInputForSelection topicStore initialChangeMap Nothing)
                  $ (\changes' changeFrameM' -> do
                        r@(w, _) <- changeInputForSelection topicStore changes' changeFrameM'
                        #show w
                        pure r
                    )
                  <$> current changes <@> changeFrameM
  let changeModifiers = switch $ snd <$> current changeWidget
      widget = fmap fst changeWidget
  changes <- accum (&) initialChangeMap changeModifiers
  pure (widget, changes)

changeInputForSelection :: ( Ord k
                           , MonadReflexGtk t m
                           )
                        => Gtk.ForestStore TopicRow
                        -> M.Map k ConcreteActivity
                        -> Maybe k
                        -> m ( Gtk.Widget
                             , Event t (M.Map k ConcreteActivity -> M.Map k ConcreteActivity)
                             )
changeInputForSelection _ _ Nothing =
  (,never) <$> runGtk (do
                          w <- notEligibleForChangeNotice
                          #show w
                          pure w
                      )
changeInputForSelection topicStore currentChanges (Just changeFrame) = do
  (newChange, activityWidget) <- newConcreteActivityWidget topicStore
                                 $ M.lookup changeFrame currentChanges
  let modifyChanges = maybe (M.delete changeFrame) (M.insert changeFrame)
                      <$> updated newChange
  runGtk $ #show activityWidget
  pure (activityWidget, modifyChanges)

setupAffectingChanges :: (MonadReflexGtk t m)
                      => Dynamic t (M.H.HashMap TopicID Topic)
                      -> Dynamic t [DaySelection]
                      -> Dynamic t (M.Map TimeFrame ConcreteActivity)
                      -> m (Dynamic t [Gtk.Widget])
setupAffectingChanges topicIndexB selection changesB = do
  let affectingChanges =
        (\sel -> M.filterWithKey (\tf a -> affectsAtLeastOneOf (Change tf a) sel))
        <$> selection <*> changesB
  label <- runGtk $ Gtk.new Gtk.Label
           [ #label Gtk.:= "The selected lessons are affected by these changes:"
           , #wrap Gtk.:= True
           ]
  runGtk $ #show label
  labelW <- runGtk $ Gtk.toWidget label
  widgets <- listWithKey affectingChanges $ \tf ac -> do
    w <- renderChange topicIndexB tf ac
    #show w
    pure w
  pure $ (labelW:) . map snd . M.toAscList <$> widgets

renderChange :: (MonadReflexGtk t m)
             => Dynamic t (M.H.HashMap TopicID Topic)
             -> TimeFrame
             -> Dynamic t ConcreteActivity
             -> m Gtk.Widget
renderChange topicIndex' timeframe activity = do
  frame <- Gtk.new Gtk.Frame [ #label Gtk.:=
                               T.L.toStrict $
                               T.L.B.toLazyText $
                               showTimeFrame timeframe <>
                               " changed to"
                             ]
  renderActivityWidget topicIndex' activity >>= sinkBin frame
  Gtk.toWidget frame

showTimeFrame :: TimeFrame -> T.L.B.Builder
showTimeFrame (FrameActivity day dayKey) =
  T.L.B.fromString (show day) <> ", hour: " <> T.L.B.fromString (show dayKey)
showTimeFrame (FrameDay day) =
  T.L.B.fromString (show day)
showTimeFrame (FrameDays start end) =
  T.L.B.fromString (show start) <> " – " <> T.L.B.fromString (show end)

renderActivityWidget :: (MonadReflexGtk t m)
                     => Dynamic t (M.H.HashMap TopicID Topic)
                     -> Dynamic t ConcreteActivity
                     -> m (Dynamic t Gtk.Widget)
renderActivityWidget topicIndex' activity =
  networkView (renderActivity topicIndex' <$> activity) >>= holdNotReadyDynamicWidget

renderActivity :: (MonadReflexGtk t m)
               => Dynamic t (M.H.HashMap TopicID Topic)
               -> ConcreteActivity
               -> m (Dynamic t Gtk.Widget)
renderActivity _ Free =
  pure <$> runGtk (do
                      w <- Gtk.new Gtk.Label [#label Gtk.:= "Free"]
                      #show w
                      Gtk.toWidget w
                  )
renderActivity _ (Cancellation reason) =
  pure <$> runGtk ( do
                      w <- Gtk.new Gtk.Label [#label Gtk.:= "Cancelled: " `T.append` reason]
                      #show w
                      Gtk.toWidget w
                  )
renderActivity _ Buffer =
  pure <$> runGtk (do
                      w <- Gtk.new Gtk.Label [#label Gtk.:= "Buffer"]
                      #show w
                      Gtk.toWidget w
                  )
renderActivity topicIndex' (Lesson topicId) = renderTopicWidget $ (M.H.! topicId) <$> topicIndex'

renderTopicWidget :: (MonadReflexGtk t m) => Dynamic t Topic -> m (Dynamic t Gtk.Widget)
renderTopicWidget topic =
  networkView ((\t -> do
                   w <- renderTopic t
                   #show w
                   pure w
               ) <$> topic) >>= holdNotReadyWidget

renderTopic :: (MonadReflexGtk t m) => Topic -> m Gtk.Widget
renderTopic (Topic [] _) = Gtk.new Gtk.Label [#label Gtk.:= "Unnamed topic"] >>= Gtk.toWidget
renderTopic (Topic [name] _) = Gtk.new Gtk.Label [#label Gtk.:= name] >>= Gtk.toWidget
renderTopic (Topic [lastName, firstName] _) = do
  box <- Gtk.new Gtk.VBox []
  Gtk.new Gtk.Label [#label Gtk.:= firstName]
    >>= \l -> #packStart box l False False 0 >> #show l
  Gtk.new Gtk.Label [#label Gtk.:= lastName]
    >>= \l -> #packStart box l False False 0 >> #show l
  Gtk.toWidget box
renderTopic (Topic (lastName:otherNames) _) = do
  outerBox <- Gtk.boxNew Gtk.OrientationVertical 0

  expander <- Gtk.new Gtk.Expander [#label Gtk.:= last otherNames]
  #packStart outerBox expander False False 0

  innerBox <- Gtk.boxNew Gtk.OrientationVertical 0
  #add expander innerBox

  mapM_ ( \name ->
           Gtk.new Gtk.Label [#label Gtk.:= name] >>= \l -> #packStart innerBox l False False 0
        ) $ reverse $ init otherNames

  lastNameLabel <-Gtk.new Gtk.Label [#label Gtk.:= lastName]
  #packStart outerBox lastNameLabel False False 0
  runGtk $ do
    #showAll expander
    #show lastNameLabel

  Gtk.toWidget outerBox

affectsAtLeastOneOf :: Change -> [DaySelection] -> Bool
affectsAtLeastOneOf = any . affects

affects :: Change -> DaySelection -> Bool
affects change daySelection@DaySelection{..} =
  change `affectsDaySelection` daySelection ||
  affectsAtLeastOneHourSelectionOf change daySelectionDay daySelectionHours

affectsDaySelection :: Change -> DaySelection -> Bool
affectsDaySelection change DaySelection{..} =
  daySelectionSelected && affectsDay change daySelectionDay

affectsDay :: Change -> Day -> Bool
affectsDay Change{..} selectedDay =
  case changeFrame of
    FrameDay frameDay -> frameDay == selectedDay
    FrameDays start end -> start <= selectedDay && selectedDay <= end
    _ -> False

affectsAtLeastOneHourSelectionOf :: Change -> Day -> V.Vector HourSelection -> Bool
affectsAtLeastOneHourSelectionOf change = V.any . affectsHourSelection change

affectsHourSelection :: Change -> Day -> HourSelection -> Bool
affectsHourSelection change@Change{..} selectedDay HourSelection{..} =
  hourSelectionSelected &&
  ( affectsDay change selectedDay ||
    case changeFrame of
      FrameActivity frameDay frameDayKey ->
        frameDay == selectedDay && hourSelectionDayKey == frameDayKey
      _ -> False
  )

orderSelection :: Gtk.ForestStore OutputRow -> [Gtk.TreePath] -> IO [DaySelection]
orderSelection outputStore selection =
  traverse (fmap fromJust . Gtk.treePathGetIndices) selection >>=
  foldlM
  (\acc path -> case path of
      di:rpath -> fromMaybe acc <$> runMaybeT
        (do
            unmodified@(_, hours, day) <-
              maybe (do
                        -- TreeSelections sometimes return nonexistent
                        -- paths in their selection for some ingenious
                        -- reason.
                        Node (DayHeader d) subRows <- MaybeT $ Gtk.treePathNewFromIndices [di]
                                                      >>= Gtk.forestStoreLookup outputStore
                        hours <- V.unsafeThaw $ V.fromList $
                                 map ( \(Node (ActivityRow _ dk enum activity) []) ->
                                         HourSelection { hourSelectionSelected = False
                                                       , hourSelectionDayKey = dk
                                                       , hourSelectionEnumerable = enum
                                                       , hourSelectionActivity = activity
                                                   }
                                     ) subRows
                        pure (False, hours, d)
                    ) pure $ M.lookup di acc
            lift $ (\v -> M.insert di v acc) <$> case rpath of
                                                   [] -> pure (True, hours, day)
                                                   [h] -> unmodified <$
                                                     V.M.unsafeModify hours
                                                     (\hs -> hs { hourSelectionSelected = True })
                                                     (fromIntegral h)
                                                   _ -> error "Output selection with length > 2"
        )
      _ -> error "Empty output selection path"
  ) M.empty >>=
  mapM (\(selected, hoursM, d) -> do
           hours <- V.unsafeFreeze hoursM
           pure $ DaySelection { daySelectionSelected = selected
                               , daySelectionHours = hours
                               , daySelectionDay = d
                               }
       ) . M.elems

data HourSelectionState = HourSelectionState !IsSingleSelection !Bool

data IsSingleSelection = NoneSelected
                       | OneSelected !DayKey
                       | MoreSelected

noneSelected :: IsSingleSelection -> Bool
noneSelected NoneSelected = True
noneSelected _ = False

eligibleForChange :: [DaySelection] -> Maybe TimeFrame
eligibleForChange [] = Nothing
eligibleForChange [DaySelection daySelected hours d] =
  let HourSelectionState singleSelectionState allSelected =
        V.foldl' (\(HourSelectionState sss as) HourSelection{..} ->
                    let singleSelectionState' = case sss of
                                                  NoneSelected ->
                                                    if hourSelectionSelected
                                                    then OneSelected hourSelectionDayKey
                                                    else NoneSelected
                                                  same@(OneSelected _) ->
                                                    if hourSelectionSelected
                                                    then MoreSelected
                                                    else same
                                                  MoreSelected -> MoreSelected
                    in HourSelectionState singleSelectionState' $
                       as && hourSelectionSelected
                )
       (HourSelectionState NoneSelected True) hours
  in if daySelected
     then if noneSelected singleSelectionState || allSelected
          then Just $ FrameDay d
          else Nothing
     else case singleSelectionState of
            OneSelected dk -> Just $ FrameActivity d dk
            _ -> Nothing
eligibleForChange xxs@(x:_) = let firstDay = daySelectionDay x
                              in if foldl' (\acc (expectedDay, ds@DaySelection{..}) -> acc &&
                                                                      fullySelected ds &&
                                                                      expectedDay == daySelectionDay
                                           ) True $ zip [firstDay..] xxs
                                 then let lastDay = daySelectionDay $ last xxs
                                      in Just $ FrameDays firstDay lastDay
                                 else Nothing

fullySelected :: DaySelection -> Bool
fullySelected DaySelection{..} = daySelectionSelected &&
                                 uncurry (||) (V.foldl' (\(as, ns) HourSelection{..} ->
                                                           ( as && hourSelectionSelected,
                                                             ns && not hourSelectionSelected
                                                           )
                                                        ) (True, True) daySelectionHours
                                              )

notEligibleForChangeNotice :: IO Gtk.Widget
notEligibleForChangeNotice = do
  label <- Gtk.new Gtk.Label [ #label Gtk.:=
                               "The set of rows you selected can not be changed as a unit. \
                               \Please select either a single activity, day or conescutive \
                               \range of days"
                             ]
  #setLineWrap label True
  Gtk.toWidget label
