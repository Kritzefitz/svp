{-# LANGUAGE OverloadedStrings, ConstraintKinds, LambdaCase, TupleSections, OverloadedLabels #-}
{-# LANGUAGE RecordWildCards, FlexibleContexts #-}
{-# OPTIONS -Wno-simplifiable-class-constraints #-}
module SVP.GUI.TopicTree ( initTopicTree
                         , TopicRow
                         , topicRowName
                         , topicRowLessons
                         , topicRowID
                         ) where

import Control.Exception (assert)
import Control.Monad ( (>=>)
                     , void
                     , when
                     )
import Control.Monad.Fix (mfix)
import Data.Function ((&))
import Data.GI.Base.Signals (disconnectSignalHandler)
import qualified Data.GI.Gtk as Gtk
import qualified Data.HashSet as S
import Data.Int (Int32)
import Data.List (isPrefixOf)
import Data.Maybe (fromJust)
import Data.Time.Format ()
import qualified Data.Text as T
import Data.Tree ( Tree(Node)
                 , subForest
                 )
import Reflex ( Dynamic
              , accum
              , mergeWith
              )
import Reflex.GI.Gtk ( MonadReflexGtk
                     , eventOnSignal
                     , runGtk
                     )

import SVP.Topic
import SVP.GUI.Common

data TopicRow = SuperTopicRow !T.Text
              | LeafTopicRow !TopicID !T.Text !Int
                deriving (Show)

topicRowID :: TopicRow -> Maybe TopicID
topicRowID (SuperTopicRow _) = Nothing
topicRowID (LeafTopicRow topicID _ _) = Just topicID

topicRowName :: TopicRow -> T.Text
topicRowName (SuperTopicRow n) = n
topicRowName (LeafTopicRow _ n _) = n

topicRowLessons :: TopicRow -> Maybe Int
topicRowLessons (SuperTopicRow _) = Nothing
topicRowLessons (LeafTopicRow _ _ l) = Just l

topicForestToRowForest :: [TopicTree] -> [Tree TopicRow]
topicForestToRowForest = map topicTreeToRowTree

topicTreeToRowTree :: TopicTree -> Tree TopicRow
topicTreeToRowTree (SuperTopic name children) = Node (SuperTopicRow name) $
                                                topicForestToRowForest children
topicTreeToRowTree (LeafTopic tID name weight) = Node (LeafTopicRow tID name weight) []

isSuperTopic :: TopicRow -> Bool
isSuperTopic (SuperTopicRow _) = True
isSuperTopic LeafTopicRow{} = False

topicRowTreeToTopicTree :: Tree TopicRow -> TopicTree
topicRowTreeToTopicTree (Node row children) = topicRowToTopicTree row $ map topicRowTreeToTopicTree children

topicRowToTopicTree :: TopicRow -> [TopicTree] -> TopicTree
topicRowToTopicTree (SuperTopicRow name) children = SuperTopic name children
topicRowToTopicTree (LeafTopicRow topicID name quantity) [] = LeafTopic topicID name quantity
topicRowToTopicTree LeafTopicRow{} _ = error "LeafTopicRow with children"

data TopicDialog =
    TopicDialog { topicDialog :: {-# UNPACK #-}!Gtk.Dialog
                , topicDialogName :: {-# UNPACK #-}!Gtk.Entry
                , topicDialogWeight :: {-# UNPACK #-}!Gtk.SpinButton
                , topicDialogApply :: {-# UNPACK #-}!Gtk.Button
                }

data TopicContextMenu =
    TopicContextMenu { topicContextMenu :: {-# UNPACK #-}!Gtk.Menu
                     , topicContextMenuEdit :: {-# UNPACK #-}!Gtk.MenuItem
                     , topicContextMenuAdd :: {-# UNPACK #-}!Gtk.MenuItem
                     , topicContextMenuAddTopic :: {-# UNPACK #-}!Gtk.MenuItem
                     , topicContextMenuAddField :: {-# UNPACK #-}!Gtk.MenuItem
                     , topicContextMenuDelete :: {-# UNPACK #-}!Gtk.MenuItem
                     , topicContextMenuDeleteTree :: {-# UNPACK #-}!Gtk.MenuItem
                     , topicContextMenuFlatten :: {-# UNPACK #-}!Gtk.MenuItem
                     }

onExistingEntries :: [TopicContextMenu -> Gtk.MenuItem]
onExistingEntries = [ topicContextMenuEdit
                    , topicContextMenuDelete
                    , topicContextMenuDeleteTree
                    , topicContextMenuFlatten
                    ]

addEntries :: [TopicContextMenu -> Gtk.MenuItem]
addEntries = [ topicContextMenuAdd
             , topicContextMenuAddTopic
             , topicContextMenuAddField
             ]

initTopicTree :: (MonadReflexGtk t m)
              => Gtk.Builder
              -> [TopicTree]
              -> m (Dynamic t [TopicTree], Gtk.ForestStore TopicRow)
initTopicTree builder initialTree = do
  store <- runGtk $ Gtk.forestStoreNewDND (topicForestToRowForest initialTree)
           (Just topicDragSourceIface) (Just topicDragDestIface)
  storeModel <- runGtk $ Gtk.toTreeModel store
  runGtk $ do
    view <- castB builder "topicTree" Gtk.TreeView
    #setReorderable view True
    setupContextMenu builder view store
    #setModel view $ Just store
    prepareCols view store
  change <- eventOnSignal storeModel #rowChanged $ \fire path _ -> do
    Just path' <- Gtk.treePathGetIndices path
    Gtk.forestStoreGetTree store path >>= fire . updateTopic path'
  insert <- eventOnSignal storeModel #rowInserted $ \fire path _ -> do
    Just path' <- Gtk.treePathGetIndices path
    Gtk.forestStoreGetTree store path >>= fire . insertTopic path'
  delete <- eventOnSignal storeModel #rowDeleted $ \fire ->
    Gtk.treePathGetIndices >=> fire . deleteTopic . fromJust
  (,store) <$> accum (&) initialTree (mergeWith (.) [insert, change, delete])

doAt :: (Integral i) => i -> ([a] -> [a]) -> [a] -> [a]
doAt i f [] = if i == 0
              then f []
              else error "doAt: Index out of bounds"
doAt i f axs@(x:xs) = case i `compare` 0 of
                    LT -> error "doAt: Negative index"
                    EQ -> f axs
                    GT -> x : doAt (pred i) f xs

updateAt :: (Integral i) => i -> (a -> a) -> [a] -> [a]
updateAt i f = doAt i $ \case
  [] -> error "updateAt: Index out of bounds"
  (x:xs) -> f x : xs

insertAt :: (Integral i) => i -> a -> [a] -> [a]
insertAt i = doAt i . (:)

deleteAt :: (Integral i) => i -> [a] -> [a]
deleteAt = flip doAt tail

updateTopic :: [Int32] -> Tree TopicRow -> [TopicTree] -> [TopicTree]
updateTopic [] _ = error "updateTopics: empty path"
updateTopic (i:is) topicRow = updateAt i $ go is
  where go [] _ = topicRowTreeToTopicTree topicRow
        go _ LeafTopic{} = error "updateTopic: Path below leaf topic"
        go is' (SuperTopic name children) = SuperTopic name $ updateTopic is' topicRow children

insertTopic :: [Int32] -> Tree TopicRow -> [TopicTree] -> [TopicTree]
insertTopic [] _ = error "insertTopic: empty path"
insertTopic [i] topicRow = insertAt i (topicRowTreeToTopicTree topicRow)
insertTopic (i:is) topicRow = updateAt i (go is)
  where go _ LeafTopic{} = error "insertTopic: Path below leaf topic"
        go is' (SuperTopic name children) = SuperTopic name $ insertTopic is' topicRow children

deleteTopic :: [Int32] -> [TopicTree] -> [TopicTree]
deleteTopic [] = error "deleteTopic: empty path"
deleteTopic [i] = deleteAt i
deleteTopic (i:is) = updateAt i (go is)
  where go _ LeafTopic{} = error "deleteTopic: Path below leaf topic"
        go is' (SuperTopic name children) = SuperTopic name $ deleteTopic is' children

prepareCols :: Gtk.TreeView
            -> Gtk.ForestStore TopicRow
            -> IO ()
prepareCols view store = do
  _ <- addTextColumn view store topicRowName "Name"
  _ <- addTextColumn view store (maybe "" (T.pack . show) . topicRowLessons) "Lessons"
  return ()

topicDragSourceIface :: Gtk.DragSourceIface Gtk.ForestStore TopicRow
topicDragSourceIface =
  Gtk.DragSourceIface { Gtk.customDragSourceRowDraggable = \_ _ -> pure True
                      , Gtk.customDragSourceDragDataGet = \model path selectionData ->
                          Gtk.treeSetRowDragData selectionData model path
                      , Gtk.customDragSourceDragDataDelete = Gtk.forestStoreRemove
                      }

topicDragDestIface :: Gtk.DragDestIface Gtk.ForestStore TopicRow
topicDragDestIface =
    Gtk.DragDestIface { Gtk.customDragDestRowDropPossible =
                            \store targetPath selectionData ->
                              Gtk.treeGetRowDragData selectionData >>= \case
                              (True, Just store', Just sourcePath) -> do
                                Just sourcePath' <- Gtk.treePathGetIndices sourcePath
                                Just targetPath' <- Gtk.treePathGetIndices targetPath
                                storeGen <- Gtk.toTreeModel store
                                let parentPath' = init targetPath'
                                    sourceStoreIsTargetStore = storeGen == store'
                                    targetIsDescendantOfSource =
                                      sourcePath' `isPrefixOf` targetPath'
                                targetIsSuperTopic <-
                                  if null parentPath'
                                  then pure True -- The root is always an implicit super topic
                                  else do
                                  parentPath <- Gtk.treePathNewFromIndices parentPath'
                                  isSuperTopic <$> Gtk.forestStoreGetValue store parentPath
                                pure
                                  $ sourceStoreIsTargetStore
                                  && not targetIsDescendantOfSource
                                  && targetIsSuperTopic
                              _ -> pure False
                      , Gtk.customDragDestDragDataReceived =
                          \targetStore targetPath selectionData ->
                            Gtk.treeGetRowDragData selectionData >>= \case
                               (True, Just sourceStore, Just sourcePath) -> do
                                 parentPath <- #copy targetPath
                                 goneUp <- #up parentPath
                                 if not goneUp
                                   then pure False
                                   else do
                                   Just position <- fmap last <$> #getIndices targetPath
                                   Gtk.castTo Gtk.CustomStore sourceStore >>=
                                     maybe (pure False)
                                     ( \(Gtk.CustomStore sourceStore') ->
                                         let sourceStore'' = Gtk.ForestStore sourceStore'
                                             Gtk.ForestStore targetStore' = targetStore
                                         in
                                           True <$
                                           ( Gtk.forestStoreGetTree
                                             (assert (sourceStore' == targetStore') sourceStore'')
                                             sourcePath >>=
                                             Gtk.forestStoreInsertTree targetStore parentPath (fromIntegral position)
                                           )
                                     )
                               (_, _, _) -> pure False
                      }

setupContextMenu :: Gtk.Builder -> Gtk.TreeView -> Gtk.ForestStore TopicRow -> IO ()
setupContextMenu builder view store = do
  menu <- castB builder "topics/context-menu" Gtk.Menu
  editEntry <- castB builder "topics/context-menu/edit" Gtk.MenuItem
  addEntry <- castB builder "topics/context-menu/add" Gtk.MenuItem
  addTopicEntry <- castB builder "topics/context-menu/add-topic" Gtk.MenuItem
  addFieldEntry <- castB builder "topics/context-menu/add-field" Gtk.MenuItem
  deleteEntry <- castB builder "topics/context-menu/delete" Gtk.MenuItem
  deleteTreeEntry <- castB builder "topics/context-menu/delete-tree" Gtk.MenuItem
  flattenEntry <- castB builder "topics/context-menu/flatten" Gtk.MenuItem
  mainWindow <- castB builder "main-window" Gtk.Window
  dialog <- setupTopicDialog mainWindow
  askDeleteDialog <- Gtk.new Gtk.MessageDialog [ #transientFor Gtk.:= mainWindow
                                               , #messageType Gtk.:= Gtk.MessageTypeQuestion
                                               , #buttons Gtk.:= Gtk.ButtonsTypeYesNo
                                               , #text Gtk.:= "Do you really want to delete this topic?"
                                               ]
  let menu' = TopicContextMenu { topicContextMenu = menu
                               , topicContextMenuEdit = editEntry
                               , topicContextMenuAdd = addEntry
                               , topicContextMenuAddTopic = addTopicEntry
                               , topicContextMenuAddField = addFieldEntry
                               , topicContextMenuDelete = deleteEntry
                               , topicContextMenuDeleteTree = deleteTreeEntry
                               , topicContextMenuFlatten = flattenEntry
                               }
  _ <- view `Gtk.on` #buttonPressEvent $ \click ->
    False <$
    ( do
        button <- Gtk.get click #button
        when (button == rightButton) $
          do
            x <- Gtk.get click #x
            y <- Gtk.get click #y
            Gtk.treeViewGetPathAtPos view (floor x) (floor y) >>=
              openContextMenu dialog menu'
              askDeleteDialog store .
              (\(_, a, _, _, _) -> a)
    )
  pure ()

rightButton :: (Num n) => n
rightButton = 3

openContextMenu :: ( Gtk.IsMessageDialog messageDialog
                   , Gtk.IsDialog messageDialog
                   , Gtk.IsWidget messageDialog
                   )
                => TopicDialog
                -> TopicContextMenu
                -> messageDialog
                -> Gtk.ForestStore TopicRow
                -> Maybe Gtk.TreePath
                -> IO ()
openContextMenu dialog menu@TopicContextMenu{..} askDeleteDialog store mPath = do
  cids <- case mPath of
            Nothing -> do
                       setOnEntries False menu
                       setAddEntries True menu
                       emptyTreePath <- Gtk.treePathNew
                       addField <- topicContextMenuAddField `Gtk.on` #activate $
                                   addSuperTopicRow store dialog emptyTreePath
                       addTopic <- topicContextMenuAddTopic `Gtk.on` #activate $
                                   addLeafTopicRow store dialog emptyTreePath
                       addFieldW <- Gtk.toWidget topicContextMenuAddField
                       addTopicW <- Gtk.toWidget topicContextMenuAddTopic
                       pure [ (addFieldW, addField)
                            , (addTopicW, addTopic)
                            ]
            Just path -> do
                       setOnEntries True menu
                       row <- Gtk.forestStoreGetValue store path
                       editCID <- topicContextMenuEdit `Gtk.on` #activate $
                                  runTopicDialog row dialog >>=
                                  mapM_ (Gtk.forestStoreSetValue store path)
                       deleteTreeCID <- topicContextMenuDeleteTree `Gtk.on` #activate $ do
                         r <- Gtk.dialogRun askDeleteDialog
                         Gtk.widgetHide askDeleteDialog
                         when (toEnum (fromIntegral r) == Gtk.ResponseTypeYes) $
                           void $ Gtk.forestStoreRemove store path
                       flattenCID <- topicContextMenuFlatten `Gtk.on` #activate $ do
                                                                 r <- Gtk.dialogRun askDeleteDialog
                                                                 Gtk.widgetHide askDeleteDialog
                                                                 when (toEnum (fromIntegral r) == Gtk.ResponseTypeYes) $ do
                                                                   current <- Gtk.forestStoreGetTree store path
                                                                   _ <- Gtk.forestStoreRemove store path
                                                                   parentPath <- #copy path
                                                                   Just path' <- #getIndices path
                                                                   True <- #up parentPath
                                                                   Gtk.forestStoreInsertForest store parentPath (fromIntegral $ last path') $ subForest current
                       cids <- case row of
                                 SuperTopicRow _ -> do
                                                 addField <- topicContextMenuAddField `Gtk.on` #activate $
                                                             addSuperTopicRow store dialog path
                                                 addTopic <- topicContextMenuAddTopic `Gtk.on` #activate $
                                                             addLeafTopicRow store dialog path
                                                 menuAddFieldW <- Gtk.toWidget topicContextMenuAddField
                                                 menuAddTopicW <- Gtk.toWidget topicContextMenuAddTopic
                                                 setAddEntries True menu
                                                 pure [ (menuAddFieldW, addField)
                                                      , (menuAddTopicW, addTopic)
                                                      ]
                                 LeafTopicRow{} -> [] <$ setAddEntries False menu
                       menuFlattenW <- Gtk.toWidget topicContextMenuFlatten
                       menuDeleteTreeW <- Gtk.toWidget topicContextMenuDeleteTree
                       menuEditW <- Gtk.toWidget topicContextMenuEdit
                       pure $ (menuFlattenW, flattenCID)
                         : (menuDeleteTreeW, deleteTreeCID)
                         : (menuEditW, editCID)
                         : cids
  #popupAtPointer topicContextMenu Nothing
  _ <- mfix $ \disconnecter ->
    Gtk.on topicContextMenu #selectionDone  $
    mapM_ (uncurry disconnectSignalHandler) cids *>
    disconnectSignalHandler topicContextMenu disconnecter
  pure ()

setEntriesSensitive :: Bool
                    -> TopicContextMenu
                    -> [TopicContextMenu -> Gtk.MenuItem]
                    -> IO ()
setEntriesSensitive sensitive menu =
    mapM_ $ \extract -> #setSensitive (extract menu) sensitive

setOnEntries :: Bool -> TopicContextMenu -> IO ()
setOnEntries sensitive menu = setEntriesSensitive sensitive menu onExistingEntries

setAddEntries :: Bool -> TopicContextMenu -> IO ()
setAddEntries sensitive menu = setEntriesSensitive sensitive menu addEntries

addSuperTopicRow :: Gtk.ForestStore TopicRow -> TopicDialog -> Gtk.TreePath -> IO ()
addSuperTopicRow = addTopicRow $ SuperTopicRow ""

addLeafTopicRow :: Gtk.ForestStore TopicRow -> TopicDialog -> Gtk.TreePath -> IO ()
addLeafTopicRow store dialog path = do
  topicID <- newTopicID store
  addTopicRow (LeafTopicRow topicID "" 0) store dialog path

newTopicID :: Gtk.ForestStore TopicRow -> IO TopicID
newTopicID store = do
  takenIDs <- foldTreeStore (\topic acc -> maybe acc (`S.insert` acc) $ topicRowID topic)
              S.empty store
  pure $ head $ filter (not . flip S.member takenIDs) $ map TopicID [0..]

foldTreeStore :: (row -> acc -> acc) -> acc -> Gtk.ForestStore row -> IO acc
foldTreeStore f initial store = do
  (worked, iter) <- Gtk.treeModelGetIterFirst store
  if worked
    then foldIter initial iter
    else pure initial
  where foldIter acc iter = do
          value <- Gtk.treeModelGetPath store iter >>= Gtk.forestStoreGetValue store
          let acc' = f value acc
          (hasChildren, childrenIter) <- Gtk.treeModelIterChildren store $ Just iter
          acc'' <- if hasChildren
                   then foldIter acc childrenIter
                   else pure acc'
          hasNext <- Gtk.treeModelIterNext store iter
          if hasNext
            then foldIter acc'' iter
            else pure acc''

addTopicRow :: TopicRow -> Gtk.ForestStore TopicRow -> TopicDialog -> Gtk.TreePath -> IO ()
addTopicRow initial store dialog path = runTopicDialog initial dialog >>=
                                        mapM_ (Gtk.forestStoreInsert store path (-1))

setupTopicDialog :: (Gtk.IsWindow window) => window -> IO TopicDialog
setupTopicDialog mainWindow = do
  builder <- builderNewFromResourceFile "interfaces/topicdialog.glade"
  dialog <- castB builder "topic-dialog" Gtk.Dialog
  Gtk.set dialog [ #transientFor Gtk.:= mainWindow ]
  nameEntry <- castB builder "name-entry" Gtk.Entry
  weightEntry <- castB builder "weight-entry" Gtk.SpinButton
  _ <- #addButton dialog ("gtk-cancel" :: T.Text) $ fromIntegral $ fromEnum Gtk.ResponseTypeCancel
  apply <- #addButton dialog ("gtk-apply" :: T.Text)
           (fromIntegral $ fromEnum Gtk.ResponseTypeApply) >>=
           Gtk.unsafeCastTo Gtk.Button
  return $ TopicDialog { topicDialog = dialog
                       , topicDialogName = nameEntry
                       , topicDialogWeight = weightEntry
                       , topicDialogApply = apply
                       }

runTopicDialog :: TopicRow -> TopicDialog -> IO (Maybe TopicRow)
runTopicDialog (SuperTopicRow name) dialog = do
  #setSensitive (topicDialogWeight dialog) False
  fmap (SuperTopicRow . fst) <$> runTopicDialog' dialog name 0
runTopicDialog (LeafTopicRow topicID name weight) dialog = do
  #setSensitive (topicDialogWeight dialog) True
  fmap (uncurry $ LeafTopicRow topicID) <$> runTopicDialog' dialog name weight

runTopicDialog' :: TopicDialog -> T.Text -> Int -> IO (Maybe (T.Text, Int))
runTopicDialog' dialog name weight = do
  #setText (topicDialogName dialog) name
  #setValue (topicDialogWeight dialog) $ fromIntegral weight
  #setSensitive (topicDialogApply dialog) $ not $ T.null name
  verifyCID <- topicDialogName dialog `Gtk.on` #changed $
               Gtk.entryGetText (topicDialogName dialog) >>=
               #setSensitive (topicDialogApply dialog) . not . T.null
  response <- #run $ topicDialog dialog
  disconnectSignalHandler (topicDialogName dialog) verifyCID
  #hide $ topicDialog dialog
  case toEnum $ fromIntegral response of
    Gtk.ResponseTypeApply -> Just <$> ( (,) <$>
                                        #getText (topicDialogName dialog) <*>
                                        ( fromIntegral <$>
                                          #getValueAsInt (topicDialogWeight dialog)
                                        )
                                      )
    _ -> pure Nothing
