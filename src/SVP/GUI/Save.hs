{-# LANGUAGE RecursiveDo, OverloadedLabels, OverloadedStrings, FlexibleContexts #-}
module SVP.GUI.Save ( new
                    , setupLoad
                    , setupSave
                    , loadSave'
                    ) where

import Control.Applicative (liftA2)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe ( MaybeT(MaybeT)
                                 , runMaybeT
                                 )
import Data.Bifunctor (first)
import qualified Data.GI.Gtk as Gtk
import qualified Data.Map.Strict as M
import Data.Maybe (isNothing)
import qualified Data.Text as T
import Data.Time ( Day
                 , getZonedTime
                 , localDay
                 , zonedTimeToLocalTime
                 )
import Data.Witherable (catMaybes)
import Reflex ( Dynamic
              , Event
              , (<@)
              , (<@>)
              , current
              , gate
              , holdDyn
              , leftmost
              , performEvent
              , performEvent_
              )
import Reflex.GI.Gtk ( ReactiveAttrOp((:==))
                     , MonadReflexGtk
                     , eventOnSignal
                     , eventOnSignal0
                     , runGtk
                     , sink
                     )
import System.IO.Error (tryIOError)

import SVP.Course
import SVP.GUI.Common
import SVP.Save
import SVP.Schedule

setupSave :: (MonadReflexGtk t m)
          => Gtk.Builder
          -> Dynamic t SVPSave
          -> Maybe FilePath
          -> SVPSave
          -> m ()
setupSave builder save initialSavePath initialSave = mdo
  toolbarSaveButton <- runGtk $ castB builder "toolbar-save" Gtk.ToolButton
  menuSaveButton <- runGtk $ castB builder "menu-save" Gtk.MenuItem
  menuSaveAtButton <- runGtk $ castB builder "menu-save-as" Gtk.MenuItem
  mainWindow <- runGtk $ castB builder "main-window" Gtk.Window
  saveDialog <- runGtk $ Gtk.new Gtk.FileChooserDialog [ #transientFor Gtk.:= mainWindow
                                                       , #action Gtk.:= Gtk.FileChooserActionSave
                                                       ]
  _ <- runGtk
       $ #addButton saveDialog "gtk-cancel" $ fromIntegral $ fromEnum Gtk.ResponseTypeCancel
  _ <- runGtk $  #addButton saveDialog "gtk-save" $ fromIntegral $ fromEnum Gtk.ResponseTypeApply
  toolbarSave <- eventOnSignal0 toolbarSaveButton #clicked
  menuSave <- eventOnSignal0 menuSaveButton #activate
  menuSaveAt <- eventOnSignal0 menuSaveAtButton #activate
  let saveLocation = fmap fst saveState
      doQuickSave = leftmost [toolbarSave, menuSave]
      doDirectSave = catMaybes $ current saveLocation <@ doQuickSave
      doFirstSave = gate (isNothing <$> current saveLocation) doQuickSave
      askSaveAt = leftmost [menuSaveAt, doFirstSave]
  doSaveAt <- catMaybes
              <$> performEvent
              (runGtk . askSaveFile saveDialog <$> current saveLocation <@ askSaveAt)
  -- We could also union doDirectSave and doSaveAt before saving, but
  -- if they fire concurrently we might as well save both concurrently.
  performEvent_ $ (\s -> runGtk . writeSave' builder s) <$> current save <@> doDirectSave
  performEvent_ $ (\s -> runGtk . writeSave' builder s) <$> current save <@> doSaveAt
  let hasBeenSaved = leftmost [doSaveAt, doDirectSave]
  saveState <- holdDyn (initialSavePath, initialSave) $
               flip (,) <$> current save <@> fmap Just hasBeenSaved
  let lastSave = fmap snd saveState
      isSaved = liftA2 (==) lastSave save
      hasUnsavedChanges = not <$> isSaved
  sink toolbarSaveButton [#sensitive :== hasUnsavedChanges]
  sink menuSaveButton [#sensitive :== hasUnsavedChanges]

writeSave' :: Gtk.Builder -> SVPSave -> FilePath -> IO ()
writeSave' builder save path = do
  mainWindow <- castB builder "main-window" Gtk.Window
  r <- tryIOError (writeSave save path)
  case r of
    Right () -> pure ()
    Left e -> reportIOError (Just mainWindow) e

askSaveFile :: Gtk.FileChooserDialog -> Maybe FilePath -> IO (Maybe FilePath)
askSaveFile fileDialog currentSaveLocation = do
  mapM_ (#setFilename fileDialog) currentSaveLocation
  askFile fileDialog

setupLoad :: (MonadReflexGtk t m) => Gtk.Builder -> m (Event t (Maybe FilePath, SVPSave))
setupLoad builder = do
  mainWindow <- runGtk $ castB builder "main-window" Gtk.Window
  loadDialog <- runGtk $ Gtk.new Gtk.FileChooserDialog [ #transientFor Gtk.:= mainWindow
                                                       , #action Gtk.:= Gtk.FileChooserActionOpen
                                                       ]
  _ <- runGtk
       $ #addButton loadDialog "gtk-cancel" $ fromIntegral $ fromEnum Gtk.ResponseTypeCancel
  _ <- runGtk $ #addButton loadDialog "gtk-open" $ fromIntegral $ fromEnum Gtk.ResponseTypeApply
  menuNewButton <- runGtk $ castB builder "menu-new" Gtk.MenuItem
  toolbarNewButton <- runGtk $ castB builder "toolbar-new" Gtk.ToolButton
  menuLoadButton <- runGtk $ castB builder "menu-open" Gtk.MenuItem
  toolbarLoadButton <- runGtk $ castB builder "toolbar-open" Gtk.ToolButton
  menuNew <- eventOnSignal menuNewButton #activate $ \fire -> new >>= fire . (,) Nothing
  toolbarNew <- eventOnSignal toolbarNewButton #clicked $ \fire -> new >>= fire . (,) Nothing
  menuLoad <- eventOnSignal menuLoadButton #activate $ \fire -> load builder loadDialog
    >>= mapM_ (fire . first Just)
  toolbarLoad <- eventOnSignal toolbarLoadButton #clicked $ \fire ->
    load builder loadDialog
    >>= mapM_ (fire . first Just)
  pure $ leftmost [ menuNew
                  , toolbarNew
                  , menuLoad
                  , toolbarLoad
                  ]

load :: Gtk.Builder -> Gtk.FileChooserDialog -> IO (Maybe (FilePath, SVPSave))
load builder loadDialog = runMaybeT $ do
  loadFrom <- MaybeT $ askFile loadDialog
  mainWindow <- lift $ castB builder "main-window" Gtk.Window
  loaded <- MaybeT $ loadSave' (Just mainWindow) loadFrom
  pure (loadFrom, loaded)

new :: IO SVPSave
new = SVPSave . newCourse . localDay . zonedTimeToLocalTime <$> getZonedTime

askFile :: Gtk.FileChooserDialog -> IO (Maybe FilePath)
askFile fileDialog = do
  response <- #run fileDialog
  #hide fileDialog
  if toEnum (fromIntegral response) == Gtk.ResponseTypeApply
    then #getFilename fileDialog
    else pure Nothing

loadSave' :: Maybe Gtk.Window -> FilePath -> IO (Maybe SVPSave)
loadSave' mainWindow path = do
  r <- tryIOError (loadSave path)
  case r of
    Left e -> reportIOError mainWindow e >> pure Nothing
    Right (Left e) -> reportError mainWindow (T.pack e) >> pure Nothing
    Right (Right course) -> pure $ Just course

newCourse :: Day -> Course
newCourse defaultDay = Course defaultDay defaultDay [] (WeekSchedule M.empty) M.empty []
