{-# LANGUAGE RecursiveDo, FlexibleContexts, OverloadedLabels, OverloadedStrings #-}

module SVP.GUI.Instance
  ( startInstance
  ) where

import qualified Data.GI.Gtk as Gtk
import qualified Data.Text.Lazy as T.L
import qualified Data.Text.Lazy.Builder as T.L.B
import Reflex ( Dynamic
              , Event
              )
import Reflex.GI.Gtk ( MonadReflexGtk
                     , eventOnSignal0
                     , runGtk
                     )
import System.FilePath (takeFileName)

import SVP.Course
import SVP.GUI.Common
import SVP.GUI.Output
import SVP.GUI.PlanPeriod
import SVP.GUI.Save
import SVP.GUI.Schedule
import SVP.GUI.TopicTree
import SVP.Save

startInstance :: (MonadReflexGtk t m)
              => Gtk.Application
              -> Maybe FilePath
              -> SVPSave
              -> m ( Event t ()
                   , Event t (Maybe FilePath, SVPSave)
                   )
startInstance application path initialSave@(SVPSave initialCourse) = do
  builder <- runGtk $ builderNewFromResourceFile "interfaces/main.glade"
  course <- instanceBusinessLogic builder initialCourse
  setupSave builder (SVPSave <$> course) path initialSave
  window <- runGtk $ castB builder "main-window" Gtk.Window
  window `Gtk.set` [#title Gtk.:= T.L.toStrict (windowTitleFromPath path)]
  closed <- eventOnSignal0 window #destroy
  #addWindow application window
  menuQuit <- runGtk $ castB builder "menu-quit" Gtk.MenuItem
  _ <- menuQuit `Gtk.on` #activate $ Gtk.mainQuit
  runGtk $ #showAll window
  loaded <- setupLoad builder
  pure (closed, loaded)

instanceBusinessLogic :: (MonadReflexGtk t m)
                      => Gtk.Builder -> Course -> m (Dynamic t Course)
instanceBusinessLogic builder initialCourse = mdo
  begin <- initPlanBegin builder $ courseBegin initialCourse
  end <- initPlanEnd builder $ courseEnd initialCourse
  (topics, topicStore) <- initTopicTree builder $ courseTopics initialCourse
  schedule <- initSchedule builder (courseFirstSchedule initialCourse)
              (courseScheduleReplacements initialCourse)
  let course = uncurry <$> (Course <$> begin <*> end <*> topics) <*> schedule <*> changes
  changes <- outputCourse builder topicStore course $ courseChanges initialCourse
  pure course

windowTitleFromPath :: Maybe String -> T.L.Text
windowTitleFromPath = T.L.B.toLazyText .
                      mappend "SVP\x2009—\x2009" .
                      maybe "new file" (\path ->
                                          T.L.B.fromString (takeFileName path) <>
                                          T.L.B.fromText " (" <>
                                          T.L.B.fromString path <>
                                          T.L.B.singleton ')'
                                       )
