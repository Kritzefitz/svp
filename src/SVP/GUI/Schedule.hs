{-# LANGUAGE OverloadedStrings, RecursiveDo, RankNTypes, OverloadedLabels, TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}
module SVP.GUI.Schedule ( initSchedule
                        ) where

import Control.Monad.IO.Class (liftIO)
import Data.Function ((&))
import qualified Data.GI.Gtk as Gtk
import qualified Data.Map.Strict as M
import Data.Maybe (fromMaybe)
import qualified Data.Sequence as Seq
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Time.LocalTime ( getZonedTime
                           , localDay
                           , zonedTimeToLocalTime
                           )
import Data.Time.Calendar (Day)
import Data.Witherable (catMaybes)
import Reflex ( Behavior
              , Dynamic
              , Event
              , (<@)
              , (<@>)
              , accum
              , current
              , listHoldWithKey
              , mergeMap
              , mergeWith
              , performEvent
              , switch
              , zipDynWith
              )
import Reflex.GI.Gtk ( MonadReflexGtk
                     , dynamicOnSignal
                     , eventOnSignal
                     , eventOnSignal0
                     , runGtk
                     , sinkBoxUniform
                     )
import Reflex.Network (networkHold)

import qualified Data.OrderedMap as M.O
import SVP.Calendar
import SVP.GUI.Common
import SVP.GUI.DatePicker
import SVP.Schedule

initSchedule :: (MonadReflexGtk t m)
             => Gtk.Builder
             -> WeekSchedule
             -> M.Map Day WeekSchedule
             -> m (Dynamic t (WeekSchedule, M.Map Day WeekSchedule))
initSchedule builder initialFirstSchedule initialScheduleReplacements = do
  schedulesBox <- runGtk $ castB builder "schedules-box" Gtk.Box
  (weekScheduleWidget, weekSchedule) <- newWeekSchedule initialFirstSchedule
  (weekScheduleReplacementsWidget, weekScheduleReplacements) <-
    newWeekScheduleReplacements builder initialScheduleReplacements
  liftIO $ do
    #packStart schedulesBox weekScheduleWidget True True 0
    #packStart schedulesBox weekScheduleReplacementsWidget True True 0
  pure $ (,) <$> weekSchedule <*> weekScheduleReplacements

newWeekScheduleReplacements :: (MonadReflexGtk t m)
                            => Gtk.Builder
                            -> M.Map Day WeekSchedule
                            -> m (Gtk.Widget, Dynamic t (M.Map Day WeekSchedule))
newWeekScheduleReplacements builder initialScheduleReplacements = mdo
  addScheduleButton <- runGtk $ castB builder "add-schedule" Gtk.Button
  dateDialog <- runGtk $ dateDialogFromBuilder builder
  addSchedule <- eventOnSignal0 addScheduleButton #clicked
  addScheduleAtM <-
    performEvent $ (\currentSchedules ->
                       liftIO getZonedTime >>=
                       runGtk
                       . runValidatedDateDialog dateDialog (not . (`M.member` currentSchedules))
                       . localDay . zonedTimeToLocalTime
                   ) <$> current scheduleReplacements <@ addSchedule
  let addScheduleAt = catMaybes addScheduleAtM
      initialWidgetArgs =
        M.fromDistinctAscList $ zip [minBound :: Word ..]
        $ M.toAscList initialScheduleReplacements
      freeKey = maybe minBound fst . M.lookupMax <$> current scheduleWidgets
      widgetUpdates = mconcat
        [ (\k d -> M.singleton k $ Just (d, WeekSchedule M.empty)) <$> freeKey <@> addScheduleAt
        , delete
        ]
  scheduleWidgets <- listHoldWithKey initialWidgetArgs widgetUpdates $ const $ \arg -> do
    r@(w, _, _) <-
      newWeekScheduleWithControls builder (M.keysSet <$> current scheduleReplacements) arg
    #show w
    pure r
  let delete = switch $ mergeMap . M.map (\(_, _, del) -> Nothing <$ del)
               <$> current scheduleWidgets
      scheduleWidgetByDay = fmap M.fromList . traverse (\(widget, dayAndSchedule, _) ->
                                                          (\(day, schedule) ->
                                                             (day, (widget, schedule))
                                                          ) <$> dayAndSchedule
                                                       ) . M.elems
                            =<< scheduleWidgets
      scheduleReplacements = M.map snd <$> scheduleWidgetByDay
  box <- runGtk $ Gtk.boxNew Gtk.OrientationVertical 0
  runGtk $ #show box
  sinkBoxUniform box (M.map fst <$> scheduleWidgetByDay) False False 0 Gtk.PackTypeStart
  (, scheduleReplacements) <$> Gtk.toWidget box

newWeekScheduleWithControls :: (MonadReflexGtk t m)
                            => Gtk.Builder
                            -> Behavior t (S.Set Day)
                            -> (Day, WeekSchedule)
                            -> m ( Gtk.Widget
                                 , Dynamic t (Day, WeekSchedule)
                                 , Event t ()
                                 )
newWeekScheduleWithControls builder occupiedDays (initialDay, initialSchedule) = do
  dateDialog <- runGtk $ dateDialogFromBuilder builder
  (redayPicker, day) <- datePickerNew dateDialog initialDay $
                        fmap (\s -> not . (`S.member` s)) occupiedDays
  deleteButton <- runGtk $ Gtk.new Gtk.Button [ #label Gtk.:= "gtk-delete"
                                              , #useStock Gtk.:= True
                                              ]
  delete <- eventOnSignal0 deleteButton #clicked
  (scheduleWidget, schedule) <- newWeekSchedule initialSchedule
  topWidget <- runGtk $ do
           topBox <- Gtk.boxNew Gtk.OrientationVertical 0
           buttonBox <- Gtk.buttonBoxNew Gtk.OrientationHorizontal
           #packStart topBox buttonBox False False 0
           #packStart topBox scheduleWidget True True 0
           #packStart buttonBox deleteButton True False 0
           #packStart buttonBox redayPicker True False 0
           #showAll buttonBox
           #show scheduleWidget
           pure topBox
  (, (,) <$> day <*> schedule, delete) <$> Gtk.toWidget topWidget

newWeekSchedule :: (MonadReflexGtk t m) => WeekSchedule -> m (Gtk.Widget, Dynamic t WeekSchedule)
newWeekSchedule (WeekSchedule initialSchedule) = do
  box <- runGtk $ Gtk.boxNew Gtk.OrientationHorizontal 0
  daySchedules <- mapM (\d -> do
                           labeledBox <- runGtk $ Gtk.boxNew Gtk.OrientationVertical 0
                           label <- runGtk $ Gtk.labelNew $ Just $ T.pack $ show d
                           (dayScheduleWidget, daySchedule) <- newDaySchedule $
                                                               fromMaybe M.O.empty $
                                                               M.lookup d initialSchedule
                           runGtk $ do
                             #packStart labeledBox label False False 0
                             #packStart labeledBox dayScheduleWidget True True 0
                             #packStart box labeledBox True True 0
                             #show labeledBox
                             #show label
                             #show dayScheduleWidget
                           pure (d, daySchedule)
                       ) [Monday .. Sunday]
  (, WeekSchedule . M.fromDistinctAscList <$> traverse sequenceA daySchedules)
    <$> Gtk.toWidget box
       

newDaySchedule :: (MonadReflexGtk t m) => DaySchedule -> m (Gtk.Widget, Dynamic t DaySchedule)
newDaySchedule initialDaySchedule = mdo
  addActivityBox <- runGtk $ Gtk.boxNew Gtk.OrientationVertical 0
  addActivityButton <- runGtk $ Gtk.new Gtk.Button [#label Gtk.:= "New activity"]
  activitiesBox <- runGtk $ Gtk.new Gtk.VBox [#valign Gtk.:= Gtk.AlignStart]
  newActivityE <- eventOnSignal0 addActivityButton #clicked
  let initialArgs = M.O.unorderMap initialDaySchedule
      freeKey = freeDayKey <$> current daySchedule
      newActivityKey = freeKey <@ newActivityE
      widgetUpdates = mconcat
        [ flip M.singleton (Just defaultActivity) <$> newActivityKey
        , delete
        ]
  unorderedActivityWidgets <- listHoldWithKey initialArgs widgetUpdates $ const $ \arg -> do
    r@(w, _, _, _) <- newScheduledActivityWithControls arg
    #show w
    pure r
  let delete =
        switch $ mergeMap . M.map (\(_, _, del, _) -> Nothing <$ del)
        <$> current unorderedActivityWidgets
  order <- accum (&) (M.O.mapOrder initialDaySchedule) $ mergeWith (.)
           [ flip (M.foldlWithKey' reorderSeq) <$> reorders
           , (\deleted -> Seq.filter (not . (`M.member` deleted))) <$> delete
           , flip (Seq.:|>) <$> newActivityKey
           ]
  let reorders = switch ( mergeMap . M.map (\(_, _, _, reorder) -> reorder)
                          <$> current unorderedActivityWidgets
                        )
      orderedActivities =
        zipDynWith
        (\o xs -> M.O.orderMap' (Seq.filter (`M.member` xs) o)
                  $ M.map (\(w, a, _, _) -> (w, a)) xs
        )
        order unorderedActivityWidgets
      daySchedule = orderedActivities >>= traverse snd
      widgets = map fst . M.O.elems <$> orderedActivities
  sinkBoxUniform activitiesBox widgets True True 0 Gtk.PackTypeStart
  liftIO $ do
    #packStart addActivityBox activitiesBox True True 0
    #packEnd addActivityBox addActivityButton False False 0
    #show activitiesBox
    #show addActivityButton
  (, daySchedule) <$> runGtk (Gtk.toWidget addActivityBox)

reorderSeq :: (Eq a) => Seq.Seq a -> a -> (Int -> Int) -> Seq.Seq a
reorderSeq s k modifyIndex =
  maybe s (\i -> Seq.insertAt (modifyIndex i) k $ Seq.deleteAt i s) $ Seq.elemIndexL k s

defaultActivity :: ScheduledActivity
defaultActivity = SimpleActivity ScheduledFree

newScheduledActivityWithControls :: (MonadReflexGtk t m)
                                 => ScheduledActivity
                                 -> m ( Gtk.Widget
                                      , Dynamic t ScheduledActivity
                                      , Event t ()
                                      , Event t (Int -> Int)
                                      )
newScheduledActivityWithControls initialActivity = do
  box <- runGtk $ Gtk.boxNew Gtk.OrientationHorizontal 0
  controlBox <- runGtk $ Gtk.buttonBoxNew Gtk.OrientationVertical
  upButton <- runGtk $ Gtk.buttonNewFromIconName (Just "go-up")
              $ fromIntegral $ fromEnum Gtk.IconSizeButton 
  deleteButton <- runGtk $ Gtk.buttonNewFromIconName (Just "edit-delete")
                  $ fromIntegral $ fromEnum Gtk.IconSizeButton 
  downButton <- runGtk $ Gtk.buttonNewFromIconName (Just "go-down")
                $ fromIntegral $ fromEnum Gtk.IconSizeButton 
  (widget, activity) <- newScheduledActivity initialActivity
  delete <- eventOnSignal0 deleteButton #clicked
  up <- (pred <$) <$> eventOnSignal0 upButton #clicked
  down <- (succ <$) <$> eventOnSignal0 downButton #clicked
  liftIO $ do
    #packStart box widget True True 0
    #show widget
    #packStart box controlBox False False 0
    #packStart controlBox upButton True True 0
    #packStart controlBox deleteButton True True 0
    #packStart controlBox downButton True True 0
    #showAll controlBox
  (, activity, delete, mergeWith (.) [up, down]) <$> runGtk (Gtk.toWidget box)

newScheduledActivity :: (MonadReflexGtk t m)
                     => ScheduledActivity -> m (Gtk.Widget, Dynamic t ScheduledActivity)
newScheduledActivity initialActivity = do
  box <- runGtk $ Gtk.boxNew Gtk.OrientationVertical 0
  alternatesToggle <- runGtk $ Gtk.checkButtonNewWithLabel "Alternates"
  let (initiallyAlternates, mkInitialWidget) =
        case initialActivity of
          SimpleActivity cAc ->
            ( False
            , do
                r@(w, _) <- newSimpleScheduledActivity cAc
                #show w
                pure r
            )
          AlternatingActivity cAcO cAcE ->
            ( True
            , do
                r@(w, _) <- newAlternatingScheduledActivity cAcO cAcE
                #show w
                pure r
            )
  runGtk $ Gtk.set alternatesToggle [#active Gtk.:= initiallyAlternates]
  alternates <- eventOnSignal alternatesToggle #toggled
                (#getActive alternatesToggle >>=)
  rec 
    widgetWithActivity <- networkHold mkInitialWidget
                          $ (\currentActivity alternates' ->
                               let (lastOdd, lastEven) = case currentActivity of
                                     SimpleActivity ac -> (ac, ScheduledFree)
                                     AlternatingActivity oddA evenA -> (oddA, evenA)
                               in do
                                 r@(w, _) <- if alternates'
                                             then newAlternatingScheduledActivity
                                                  lastOdd lastEven
                                             else newSimpleScheduledActivity lastOdd
                                 #show w
                                 pure r
                            ) <$> current activity <@> alternates
    let activity = widgetWithActivity >>= snd
  alternatesToggleW <- runGtk $ Gtk.toWidget alternatesToggle
  runGtk $ #show alternatesToggleW
  let boxWidgets = (\(w, _) -> [alternatesToggleW, w]) <$> widgetWithActivity
  sinkBoxUniform box boxWidgets True True 0 Gtk.PackTypeStart
  (, activity) <$> runGtk (Gtk.toWidget box)


newAlternatingScheduledActivity :: (MonadReflexGtk t m)
                                => ConcreteScheduledActivity
                                -> ConcreteScheduledActivity
                                -> m (Gtk.Widget, Dynamic t ScheduledActivity)
newAlternatingScheduledActivity initialOdd initialEven = do
  box <- runGtk $ Gtk.boxNew Gtk.OrientationHorizontal 0
  oddBox <- runGtk $ Gtk.boxNew Gtk.OrientationVertical 0
  oddLabel <- runGtk $ Gtk.labelNew $ Just "Odd weeks"
  (oddWidget, oddActivity) <- newConcreteScheduledActivity initialOdd
  evenBox <- runGtk $ Gtk.boxNew Gtk.OrientationVertical 0
  evenLabel <- runGtk $ Gtk.labelNew $ Just "Even weeks"
  (evenWidget, evenActivity) <- newConcreteScheduledActivity initialEven
  liftIO $ do
    #packStart box oddBox True True 0
    #packStart oddBox oddLabel False False 0
    #packStart oddBox oddWidget False False 0
    #showAll oddBox
    #packStart box evenBox True True 0
    #packStart evenBox evenLabel False False 0
    #packStart evenBox evenWidget False False 0
    #showAll evenBox
  (, AlternatingActivity <$> oddActivity <*> evenActivity) <$> Gtk.toWidget box

newSimpleScheduledActivity :: (MonadReflexGtk t m)
                           => ConcreteScheduledActivity
                           -> m (Gtk.Widget, Dynamic t ScheduledActivity)
newSimpleScheduledActivity initial = fmap (fmap SimpleActivity) <$>
                                     newConcreteScheduledActivity initial

newConcreteScheduledActivity :: (MonadReflexGtk t m)
                             => ConcreteScheduledActivity
                             -> m (Gtk.Widget, Dynamic t ConcreteScheduledActivity)
newConcreteScheduledActivity initial = do
  isExerciseToggle <- runGtk $ Gtk.checkButtonNewWithLabel "Is lesson"
  let initiallyOn = case initial of
                      ScheduledLesson -> True
                      ScheduledFree -> False
  runGtk $ #setActive isExerciseToggle initiallyOn
  isExercise <- dynamicOnSignal initiallyOn isExerciseToggle #toggled
                (#getActive isExerciseToggle >>=)
  let activity = (\isExercise' -> if isExercise'
                                  then ScheduledLesson
                                  else ScheduledFree) <$> isExercise
  (, activity) <$> Gtk.toWidget isExerciseToggle
