{-# LANGUAGE OverloadedLabels, OverloadedStrings #-}
{-# OPTIONS -Wno-simplifiable-class-constraints #-}
module SVP.GUI.Common ( addTextCellRenderer
                      , addTextColumn
                      , builderNewFromResourceFile
                      , castB
                      , loadInterfaceResourceFile
                      , reportError
                      , reportIOError
                      ) where

import Paths_svp (getDataFileName)
import Control.Exception ( Exception( displayException
                                    )
                         , throwIO
                         )
import Control.Monad (void)
import qualified Data.GI.Gtk as Gtk
import qualified Data.Text as T

addTextColumn :: ( Gtk.IsTreeView view
                 , Gtk.IsTreeModel (model row)
                 , Gtk.IsTypedTreeModel model
                 )
              => view
              -> model row
              -> (row -> T.Text)
              -> T.Text
              -> IO Gtk.TreeViewColumn
addTextColumn view model f title = do
  col <- Gtk.new Gtk.TreeViewColumn [ #expand Gtk.:= True ]
  rend <- Gtk.new Gtk.CellRendererText [ #text Gtk.:= title ]
  #packStart col rend True
  Gtk.cellLayoutSetAttributes col rend model $ \row -> [ #text Gtk.:= f row ]
  _ <- Gtk.treeViewAppendColumn view col
  return col

addTextCellRenderer :: ( Gtk.IsCellLayout cellLayout
                       , Gtk.IsTreeModel (model row)
                       , Gtk.IsTypedTreeModel model
                       )
                    => cellLayout
                    -> model row
                    -> (row -> T.Text)
                    -> IO Gtk.CellRendererText
addTextCellRenderer cellLayout model f = do
  renderer <- Gtk.new Gtk.CellRendererText []
  Gtk.cellLayoutSetAttributes cellLayout renderer model $
    \row -> [ #text Gtk.:= f row]
  Gtk.cellLayoutPackStart cellLayout renderer True
  pure renderer

builderNewFromResourceFile :: FilePath -> IO Gtk.Builder
builderNewFromResourceFile file =
  getDataFileName file >>= Gtk.builderNewFromFile . T.pack

loadInterfaceResourceFile :: Gtk.Builder -> String -> IO ()
loadInterfaceResourceFile builder filename =
  void $ getDataFileName filename >>= #addFromFile builder . T.pack

newtype UnknownBuilderIdentifierException =
  UnknownBuilderIdentifierException T.Text
  deriving (Show)

instance Exception UnknownBuilderIdentifierException where
  displayException (UnknownBuilderIdentifierException identifier) =
    "Unknown identifier " ++ show identifier ++ " given to castB"

castB :: ( Gtk.IsBuilder builder
         , Gtk.ManagedPtrNewtype o
         , Gtk.TypedObject o
         )
      => builder -> T.Text -> (Gtk.ManagedPtr o -> o) -> IO o
castB builder identifier constructor = do
  Gtk.builderGetObject builder identifier
    >>= maybe (throwIO $ UnknownBuilderIdentifierException identifier)
    (Gtk.unsafeCastTo constructor)
  
reportIOError :: Maybe Gtk.Window -> IOError -> IO ()
reportIOError mainWindow e = reportError mainWindow $ T.pack $ displayException e

reportError :: Maybe Gtk.Window -> T.Text -> IO ()
reportError mainWindow e = do
  errorMessage <- Gtk.new Gtk.MessageDialog [ #messageType Gtk.:= Gtk.MessageTypeError
                                            , #buttons Gtk.:= Gtk.ButtonsTypeOk
                                            , #text Gtk.:= e
                                            ]
  #setTransientFor errorMessage mainWindow
  _ <- #run errorMessage
  #destroy errorMessage
