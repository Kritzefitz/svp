{-# LANGUAGE OverloadedStrings, RecursiveDo, OverloadedLabels, FlexibleContexts #-}
{-# OPTIONS -Wno-simplifiable-class-constraints #-}
module SVP.GUI.DatePicker ( DateDialog
                          , dateDialogFromBuilder
                          , runDateDialog
                          , runValidatedDateDialog
                          , datePickerNew
                          , datePickerFromButton
                          ) where

import Data.GI.Base.Signals (disconnectSignalHandler)
import qualified Data.GI.Gtk as Gtk
import qualified Data.Text as T
import Data.Time.Calendar ( Day
                          , fromGregorian
                          , toGregorian
                          )
import Reflex ( Behavior
              , Dynamic
              , (<@)
              , current
              , fmapMaybe
              , holdDyn
              , performEvent
              )
import Reflex.GI.Gtk ( MonadReflexGtk
                     , ReactiveAttrOp((:==))
                     , eventOnSignal0
                     , runGtk
                     , sink
                     )

import SVP.GUI.Common

calendarGetDay :: Gtk.IsCalendar calendar =>
                  calendar -> IO Day
calendarGetDay calendar = do
  (y, m, d) <- Gtk.calendarGetDate calendar
  pure $ fromGregorian (fromIntegral y) (fromIntegral m+1) $ fromIntegral d

data DateDialog = DateDialog { dateDialogDialog :: {-# UNPACK #-}!Gtk.Dialog
                             , dateDialogCalendar :: {-# UNPACK #-}!Gtk.Calendar
                             , dateDialogOk :: {-# UNPACK #-}!Gtk.Button
                             }

dateDialogFromBuilder :: Gtk.Builder -> IO DateDialog
dateDialogFromBuilder builder = do
  dialog <- castB builder "date-picker-dialog" Gtk.Dialog
  calendar <- castB builder "date-picker" Gtk.Calendar
  ok <- castB builder "date-picker-ok" Gtk.Button
  pure $ DateDialog { dateDialogDialog = dialog
                    , dateDialogCalendar = calendar
                    , dateDialogOk = ok
                    }

runDateDialog :: DateDialog -> Day -> IO (Maybe Day)
runDateDialog dialog = runValidatedDateDialog dialog (const True)

runValidatedDateDialog :: DateDialog
                       -> (Day -> Bool)
                       -> Day
                       -> IO (Maybe Day)
runValidatedDateDialog (DateDialog dialog calendar ok) validate currentDay = do
  let (year, month, day) = toGregorian currentDay
  #selectMonth calendar (fromIntegral month - 1) $ fromIntegral year
  #selectDay calendar $ fromIntegral day
  #setSensitive ok $ validate currentDay
  validateSignal <- Gtk.on calendar #daySelected $
                    calendarGetDay calendar >>= #setSensitive ok . validate
  response <- toEnum . fromIntegral <$> #run dialog
  disconnectSignalHandler calendar validateSignal
  #hide dialog
  case response of
    Gtk.ResponseTypeOk -> Just <$> calendarGetDay calendar
    _ -> pure Nothing

datePickerNew :: (MonadReflexGtk t m)
              => DateDialog
              -> Day
              -> Behavior t (Day -> Bool)
              -> m (Gtk.Widget, Dynamic t Day)
datePickerNew dateDialog initialDay validate = do
  button <- runGtk $ Gtk.new Gtk.Button []
  (,)
    <$> runGtk (Gtk.toWidget button)
    <*> datePickerFromButton dateDialog button initialDay validate

datePickerFromButton :: (MonadReflexGtk t m)
                     => DateDialog
                     -> Gtk.Button
                     -> Day
                     -> Behavior t (Day -> Bool)
                     -> m (Dynamic t Day)
datePickerFromButton dateDialog openDialog initialDay validate = do
  open <- eventOnSignal0 openDialog #clicked
  rec
    selected <- performEvent
              $ (\validate' -> runGtk . runValidatedDateDialog dateDialog validate')
              <$> validate
              <*> current value
              <@ open
    let reallySelected = fmapMaybe id selected
    value <- holdDyn initialDay reallySelected
  sink openDialog [#label :== T.pack . show <$> value]
  pure value
