{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}
module SVP.GUI.PlanPeriod ( initPlanBegin
                          , initPlanEnd
                          ) where

import qualified Data.GI.Gtk as Gtk
import Data.Time.Calendar (Day)
import Control.Monad.IO.Class (liftIO)
import Reflex (Dynamic)
import Reflex.GI.Gtk (MonadReflexGtk)

import SVP.GUI.Common
import SVP.GUI.DatePicker

initPlanBegin :: (MonadReflexGtk t m) => Gtk.Builder -> Day -> m (Dynamic t Day)
initPlanBegin b initialDay = do
  dialog <- liftIO $ dateDialogFromBuilder b
  button <- liftIO $ castB b "open-from-dialog" Gtk.Button
  datePickerFromButton dialog button initialDay (pure $ const True)

initPlanEnd :: (MonadReflexGtk t m) => Gtk.Builder -> Day -> m (Dynamic t Day)
initPlanEnd b initialDay = do
  dialog <- liftIO $ dateDialogFromBuilder b
  button <- liftIO $ castB b "open-until-dialog" Gtk.Button
  datePickerFromButton dialog button initialDay (pure $ const True)
