module Data.OrderedMap ( OrderedMap
                       , orderMap
                       , orderMap'
                       , unorderMap
                       , mapOrder
                       , (!)
                       , adjust
                       , append
                       , delete
                       , elems
                       , elemIndex
                       , empty
                       , foldlWithKey
                       , foldlWithKey'
                       , foldrWithKey
                       , foldrWithKey'
                       , fromList
                       , insert
                       , keys
                       , lookup
                       , map
                       , mapWithKey
                       , member
                       , move
                       , prepend
                       , singleton
                       , toList
                       , traverseWithKey
                       , update
                       , updateWith
                       ) where

import Prelude hiding ( lookup
                      , map
                      )
import qualified Data.Foldable as F
import qualified Data.Map.Strict as M
import Data.Maybe (fromMaybe)
import qualified Data.Sequence as S
import Data.Serialize ( Serialize
                      , get
                      , put
                      )
import GHC.Stack (HasCallStack)

data OrderedMap k v = OrderedMap
  { mapOrder :: !(S.Seq k)
  , unorderMap :: !(M.Map k v)
  }

instance (Ord k, Serialize k, Serialize v) => Serialize (OrderedMap k v) where
    get = fmap fromList get
    put = put . toList

orderMap :: (Ord k) => S.Seq k -> M.Map k v -> Maybe (OrderedMap k v)
orderMap order xs = if all (`M.member` xs) order
                       then Just $ OrderedMap
                            { mapOrder = order
                            , unorderMap = xs
                            }
                       else Nothing

orderMap' :: (Ord k, HasCallStack) => S.Seq k -> M.Map k v -> OrderedMap k v
orderMap' order xs = fromMaybe (error "orderMap': order contains unknown key")
                     $ orderMap order xs

empty :: OrderedMap k v
empty = OrderedMap S.empty M.empty

singleton :: (Ord k) => k -> v -> OrderedMap k v
singleton k v = OrderedMap (S.singleton k) (M.singleton k v)

generalAdd :: (Ord k) => (k -> S.Seq k -> S.Seq k)
           -> k -> v -> OrderedMap k v -> OrderedMap k v
generalAdd add k v m@(OrderedMap order index) = if k `M.member` index
                                                then m
                                                else OrderedMap (add k order) (M.insert k v index)

append :: (Ord k) => k -> v -> OrderedMap k v -> OrderedMap k v
append = generalAdd $ flip (S.|>)

prepend :: (Ord k) => k -> v -> OrderedMap k v -> OrderedMap k v
prepend = generalAdd (S.<|)

seqInsert :: Int -> a -> S.Seq a -> S.Seq a
seqInsert i x xs = let (front, back) = S.splitAt i xs
                   in (front S.|> x) S.>< back

insert :: (Ord k) => Int -> k -> v -> OrderedMap k v -> OrderedMap k v
insert = generalAdd . seqInsert

seqMove :: Int -> Int -> S.Seq a -> S.Seq a
seqMove from to xs = let (front, back') = S.splitAt from xs
                     in case S.viewl back' of
                          S.EmptyL -> front
                          x S.:< back -> if to < 0
                                         then seqInsert (S.length front + to) x front S.>< back
                                         else front S.>< seqInsert to x back

move :: (Eq k) => k -> Int -> OrderedMap k v -> OrderedMap k v
move k i (OrderedMap order index) = OrderedMap newOrder index
    where newOrder = maybe order (\oldI -> seqMove oldI i order) $ S.elemIndexL k order

update :: (Ord k) => k -> v -> OrderedMap k v -> OrderedMap k v
update k v m@(OrderedMap order index) = if k `M.member` index
                                        then OrderedMap order (M.insert k v index)
                                        else m

updateWith :: (Ord k) => (v -> v -> v) -> k -> v -> OrderedMap k v -> OrderedMap k v
updateWith f k v m@(OrderedMap order index) = if k `M.member` index
                                              then OrderedMap order (M.insertWith f k v index)
                                              else m

delete :: (Ord k) => k -> OrderedMap k v -> OrderedMap k v
delete k (OrderedMap order index) = OrderedMap (S.filter (/= k) order) (M.delete k index)

adjust :: (Ord k) => (v -> v) -> k -> OrderedMap k v -> OrderedMap k v
adjust f k (OrderedMap order index) = OrderedMap order $ M.adjust f k index

keys :: OrderedMap k v -> [k]
keys (OrderedMap order _) = F.toList order

elems :: (Ord k) => OrderedMap k v -> [v]
elems = F.toList

toList :: (Ord k) => OrderedMap k v -> [(k, v)]
toList = foldrWithKey (\k v acc -> (k,v):acc) []

fromList :: (Ord k) => [(k, v)] -> OrderedMap k v
fromList = foldr (uncurry prepend) empty

member :: (Ord k) => k -> OrderedMap k v -> Bool
member k (OrderedMap _ index) = k `M.member` index

lookup :: (Ord k) => k -> OrderedMap k v -> Maybe v
lookup k (OrderedMap _ index) = M.lookup k index

(!) :: (Ord k) => OrderedMap k v -> k -> v
(!) (OrderedMap _ index) k = index M.! k

elemIndex :: (Eq k) => k -> OrderedMap k v -> Maybe Int
elemIndex k (OrderedMap order _) = S.elemIndexL k order

instance (Ord k, Show k, Show v) => Show (OrderedMap k v) where
    showsPrec x m = showParen (x>10) $
                    showString "fromList " . shows (toList m)

map :: (a -> b) -> OrderedMap k a -> OrderedMap k b
map = fmap

mapWithKey :: (k -> a -> b) -> OrderedMap k a -> OrderedMap k b
mapWithKey f (OrderedMap order index) = OrderedMap order $ M.mapWithKey f index

instance Functor (OrderedMap k) where
    fmap f (OrderedMap order index) = OrderedMap order $ fmap f index

liftFoldl :: (Ord k) =>
             ((a -> k -> a) -> a -> S.Seq k -> a)
          -> (a -> k -> v -> a)
          -> a
          -> OrderedMap k v
          -> a
liftFoldl foldlF f initial (OrderedMap order index) =
    foldlF (\acc k -> f acc k $ index M.! k) initial order

liftFoldr :: (Ord k) =>
            ((k -> a -> a) -> a -> S.Seq k -> a)
         -> (k -> v -> a -> a)
         -> a
         -> OrderedMap k v
         -> a
liftFoldr foldrF f initial (OrderedMap order index) =
    foldrF (\k acc -> f k (index M.! k) acc) initial order

foldlWithKey :: (Ord k) => (a -> k -> v -> a) -> a -> OrderedMap k v -> a
foldlWithKey = liftFoldl foldl

foldlWithKey' :: (Ord k) => (a -> k -> v -> a) -> a -> OrderedMap k v -> a
foldlWithKey' = liftFoldl F.foldl'

foldrWithKey :: (Ord k) => (k -> v -> a -> a) -> a -> OrderedMap k v -> a
foldrWithKey = liftFoldr foldr

foldrWithKey' :: (Ord k) => (k -> v -> a -> a) -> a -> OrderedMap k v -> a
foldrWithKey' = liftFoldr F.foldr'

instance (Ord k) => Foldable (OrderedMap k) where
    foldl = foldlWithKey . (\f acc _ v -> f acc v)
    foldl' = foldlWithKey' . (\f acc _ v -> f acc v)
    foldr = foldrWithKey . const
    foldr' = foldrWithKey' . const
    null (OrderedMap order _) = S.null order
    length (OrderedMap order _) = S.length order
    v `elem` (OrderedMap _ index) = v `elem` index
    maximum (OrderedMap _ index) = maximum index
    minimum (OrderedMap _ index) = minimum index
    sum (OrderedMap _ index) = sum index
    product (OrderedMap _ index) = product index

traverseWithKey :: (Ord k, Applicative f) =>
                   (k -> a -> f b) -> OrderedMap k a -> f (OrderedMap k b)
traverseWithKey f (OrderedMap order index) = OrderedMap order <$> M.traverseWithKey f index

instance (Ord k) => Traversable (OrderedMap k) where
    traverse = traverseWithKey . (\f _ v -> f v)

instance (Ord k, Eq v) => Eq (OrderedMap k v)  where
    (OrderedMap order1 _) == (OrderedMap order2 _) = order1 == order2
