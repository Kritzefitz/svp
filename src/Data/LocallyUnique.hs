{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Data.LocallyUnique ( LocalKey
                          , newLocalKey
                          , newLocalKeys
                          ) where

import Data.Hashable (Hashable)
import Data.Serialize (Serialize)

newtype LocalKey = LocalKey Word
  deriving (Show, Eq, Ord, Hashable, Serialize)

newLocalKey :: (LocalKey -> Bool) -> LocalKey
newLocalKey isFree = case newLocalKeys isFree of
                       [] -> error "newLocalKey: Key space exhausted"
                       x:_ -> x

newLocalKeys :: (LocalKey -> Bool) -> [LocalKey]
newLocalKeys isFree = filter isFree $ map LocalKey [0..maxBound]
