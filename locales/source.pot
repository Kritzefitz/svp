# SVP source code translation file.
# Copyright (C) 2014 Sven Bartscher, Thomas Bartscher
# Sven Bartscher <sven.bartscher@weltraumschlangen.de>, 2014.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: SVP 0.1.0\n"
"Report-Msgid-Bugs-To: sven.bartscher@weltraumschlangen.de\n"
"POT-Creation-Date: 2009-01-13 06:05-0800\n"
"PO-Revision-Date: 2014-06-05 11:16+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: SVP/Activity.hs:0
msgid "free hour"
msgstr ""

#: SVP/Activity.hs:0
msgid "%s - %s with %s"
msgstr ""

#: SVP/GUI/WeekTable.hs:0
msgid "Weekplan"
msgstr ""

#: SVP/GUI/TopicTree.hs:0
msgid "Topics"
msgstr ""

#: SVP/GUI/TopicTree.hs:0
msgid "Weight"
msgstr ""
